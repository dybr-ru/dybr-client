# Dybr Client

Dybr.ru React application that creates an SPA that allows people to write stuff in their personal blogs and read other people's stuff.

See it in action: https://dybr.ru

## Start developing

You are a brave person. Let's get to it.

### Install:

- [git](https://www.atlassian.com/git/tutorials/install-git)
- [node.js](https://nodejs.org/en/)
- [vscode](https://code.visualstudio.com/) or WebStorm
- DevTools are absolutely required to debug the application. Feel free to use Firefox or Chrome. Both have good DevTools.

### Make it work:

In the console, go to a folder where you want to have the project. Run these commands:

    git clone git@gitlab.com:dybr-ru/dybr-client.git
    cd dybr-client
    git checkout develop
    npm install
    npm start

This should open a browser tab with a local instance of the client. It is connected to a test server at http://slonopotam.net. You should see latest dybr news in the bottom of the main page. If it says something else, that means you have no access, go to our dev chat and ask for it.

### Style
If you use Webstorm, it is recommended to set up [file nesting](https://www.jetbrains.com/help/webstorm/file-nesting-dialog.html) for our style files.

Add `.module.css; .style.js` extensions to `.js` nesting rules. 

If you use vscode, you can go to [this feature request](https://github.com/microsoft/vscode/issues/6328) and put more likes there. Until they implement File Nesting you will need to scroll a bit more files than lucky guys with Webstorm. Sorry.

### Things that will help in your journey:

* [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)
* VSCode plugins:
    * **prettier (a MUST)**
    * git blame / git history
    * vscode-icons
* **_Ask stupid questions_**
