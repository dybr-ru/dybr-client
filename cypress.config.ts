import { defineConfig } from 'cypress';

import setupNodeEvents from './cypress/plugins/index.js';

export default defineConfig({
  reporter: 'junit',
  reporterOptions: {
    mochaFile: 'cypress/test-results/TEST-[hash].xml'
  },
  chromeWebSecurity: false,
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents,
    baseUrl: 'http://localhost:3002'
  }
});
