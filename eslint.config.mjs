import {
  fixupConfigRules,
  fixupPluginRules,
  includeIgnoreFile
} from '@eslint/compat';
import { FlatCompat } from '@eslint/eslintrc';
import js from '@eslint/js';
import tsParser from '@typescript-eslint/parser';
import react from 'eslint-plugin-react';
import reactHooks from 'eslint-plugin-react-hooks';
import globals from 'globals';
import path from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
  baseDirectory: __dirname,
  recommendedConfig: js.configs.recommended,
  allConfig: js.configs.all
});
const gitignorePath = path.resolve(__dirname, '.gitignore');

export default [
  includeIgnoreFile(gitignorePath),
  {
    ignores: ['**/Froala/**']
  },
  ...fixupConfigRules(
    compat.extends(
      'eslint:recommended',
      'plugin:react/recommended',
      'plugin:react-hooks/recommended',
      'prettier',
      'plugin:@dword-design/import-alias/recommended'
    )
  ),
  {
    plugins: {
      react: fixupPluginRules(react),
      'react-hooks': fixupPluginRules(reactHooks)
    },

    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node,
        ...globals.jest
      },

      parser: tsParser,
      ecmaVersion: 2020,
      sourceType: 'module',

      parserOptions: {
        ecmaFeatures: {
          jsx: true,
          modules: true
        }
      }
    },

    settings: {
      react: {
        version: 'detect'
      }
    },

    rules: {
      'no-unused-expressions': [
        'error',
        {
          allowShortCircuit: true,
          allowTernary: true
        }
      ],
      'react-hooks/rules-of-hooks': 'error',
      'react-hooks/exhaustive-deps': 'off',
      'react/react-in-jsx-scope': 'off',
      'no-console': 'off',
      'no-unused-vars': [
        'warn',
        {
          caughtErrors: 'none'
        }
      ],
      'prefer-reflect': 'off',
      radix: 'off',
      'react/sort-comp': 'off',
      'react/jsx-boolean-value': 'off',
      'react/prop-types': 'off',
      'no-use-before-define': 'warn',
      'no-nested-ternary': 'off',
      'no-alert': 'off',
      eqeqeq: 'off',
      'new-cap': 'off',
      'max-depth': 'off',
      '@dword-design/import-alias/prefer-alias': [
        'error',
        {
          alias: {
            '@': './src'
          },
          aliasForSubpaths: true,
          allowSubpathWithAlias: false
        }
      ]
    }
  },
  {
    files: ['**/*.jsx', '**/*.js', '**/*.ts', '**/*.tsx']
  }
];
