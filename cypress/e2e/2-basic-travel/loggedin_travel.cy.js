describe('User travels through pages', () => {
  it('User travels through basic pages', function () {
    cy.loginUI();
    cy.getByTestId('new-entry-link').click();
    cy.location().should(function (location) {
      expect(location.hash).to.be.empty;
      expect(location.pathname).to.eq(`/blog/${Cypress.env('slug')}/0/edit`);
      expect(location.search).to.be.empty;
    });
    cy.getByTestId('new-entry-topic').type('entry name');
    cy.getByTestId('my-blog-link').click();
    cy.location().should(function (location) {
      expect(location.hash).to.be.empty;
      expect(location.pathname).to.eq(`/blog/${Cypress.env('slug')}`);
      expect(location.search).to.be.empty;
    });
    cy.getByTestId('my-favorites-link').click();
    cy.location().should(function (location) {
      expect(location.hash).to.be.empty;
      expect(location.pathname).to.eq(`/blog/${Cypress.env('slug')}/favorites`);
      expect(location.search).to.be.empty;
    });
    cy.getByTestId('blog-my-bookmarks-link').click();
    cy.location().should(function (location) {
      expect(location.hash).to.be.empty;
      expect(location.pathname).to.eq(`/blog/${Cypress.env('slug')}/bookmarks`);
      expect(location.search).to.be.empty;
    });
    cy.getByTestId('blog-my-favorites-link').click();

    cy.getByTestId('my-notifications-button').click();
    cy.getByTestId('my-quick-access-link').click();
    cy.location().should(function (location) {
      expect(location.hash).to.be.empty;
      expect(location.pathname).to.eq('/feed');
      expect(location.search).to.be.empty;
    });

    cy.getByTestId('user-menu-button').click();
    cy.getByTestId('sidebar-my-active-profile-link').click();
    cy.getByTestId('tab-favorites').click();
    cy.getByTestId('tab-readers').click();

    cy.getByTestId('user-menu-button').click();
    cy.getByTestId('sidebar-settings-link').click();
    cy.location().should(function (location) {
      expect(location.hash).to.be.empty;
      expect(location.pathname).to.eq('/settings/common');
      expect(location.search).to.be.empty;
    });
    cy.getByTestId('tab-profile').click();
    cy.getByTestId('tab-privacy').click();
    cy.getByTestId('tab-blog').click();
    cy.getByTestId('tab-common').click();

    cy.getByTestId('user-menu-button').click();
    cy.getByTestId('sidebar-create-new-profile-link').click();

    cy.getByTestId('user-menu-button').click();
    cy.getByTestId('sidebar-help-link').click();
    cy.getByTestId('user-menu-button').click();
    cy.getByTestId('sidebar-errorlog-link').click();
    cy.getByTestId('user-menu-button').click();
    cy.getByTestId('sidebar-terms-link').click();
    cy.getByTestId('user-menu-button').click();
    cy.getByTestId('sidebar-logout-link').click();
  });
});
