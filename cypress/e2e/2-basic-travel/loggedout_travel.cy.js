describe('Guest user travels through pages', () => {
  it('Opens main page', function () {
    // cy.clearLocalStorage();
    cy.visit(Cypress.env('baseUrl'));
    cy.title().should('include', 'Дыбр');
    cy.get('[data-testid=header]');
    cy.contains('для всех и всего');
    cy.get('[data-testid=login-form]');
    cy.get('[data-testid=current]');
    cy.get('[data-testid=footer]');
  });

  it('Opens about page', function () {
    cy.visit(Cypress.env('baseUrl') + '/about');
    cy.get('[data-testid=headerloggedout]');

    cy.get('[data-testid=about-section]');
    cy.get('[data-testid=dybr-team-img]');
    cy.get('[data-testid=footer]');
  });
  it('Opens contact page', function () {
    cy.visit(Cypress.env('baseUrl') + '/contacts');

    cy.get('[data-testid=contacts]');
    cy.get('[data-testid=footer]');
  });
  it('Opens terms and conditions page', function () {
    cy.visit(Cypress.env('baseUrl') + '/terms');

    cy.get('[data-testid=terms]');
    cy.get('[data-testid=footer]');
  });

  // todo rewrite taking into account empty DB
  // it('Opens blog', function() {
  //   cy.visit(Cypress.env('baseUrl') + '/blog/dybr');
  //   cy.get('[data-testid=blog]');
  //   cy.get('[data-testid=footer]');
  // });
  //
  // it('Opens blog entry', function() {
  //   cy.visit(Cypress.env('baseUrl') + '/blog/dybr/2514279');
  //   cy.get('[data-testid=headerloggedout]');
  //   cy.get('[data-testid=blog-entry]');
  //   cy.get('[data-testid=footer]');
  // });

  it('Does not open settings', function () {
    cy.visit(Cypress.env('baseUrl') + '/settings');
    // redirects to home
    cy.contains('для всех и всего');
    cy.location().should(function (location) {
      expect(location.hash).to.be.empty;
      expect(location.pathname).to.eq('/');
      expect(location.search).to.be.empty;
    });
  });
});
