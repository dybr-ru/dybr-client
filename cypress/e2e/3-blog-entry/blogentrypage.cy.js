describe('User opens blog entry page', () => {
  // todo rewrite for empty environment
  it.skip('User travels through basic functionality', function () {
    cy.loginUI();
    const eid = '2458299';
    cy.visit(`${Cypress.env('baseUrl')}/blog/dybr/${eid}`);

    cy.getByTestId('blog-entry-title-link').contains('Кросспост');
    cy.getByTestId('pagination-top').contains('следующая').click();
    cy.getByTestId('pagination-top').contains('предыдущая').click();
    cy.getByTestId('pagination-bottom').contains('2').click();
    cy.getByTestId('pagination-bottom').contains('3').click();
    cy.getByTestId('pagination-bottom').contains('1').click();
    cy.wait(1000);
    cy.getByTestId('blog-comment').contains('Спасибо вам!');
    cy.getByTestId('blog-entry-comment-reply').first().click();
    cy.getByTestId('froala-editor-area').type('hello');
    cy.getByTestId('blog-entry-comment-button')
      .contains('комментировать')
      .click();
    cy.getByTestId('blog-entry-comment-button').contains('публикуем');
    cy.location().should((location) => {
      expect(location.hash).to.be.a('string');
      expect(location.pathname).to.eq(`/blog/dybr/${eid}`);
      expect(location.search).to.eq('?page=4');
    });
    //   .trigger('mousedown')
    //   .type('{selectall}{rightarrow}{rightarrow}')
    //   .trigger('mouseup');
    //
    // cy.document().trigger('selectionchange');

    cy.wait(1000);

    // cy.getByTestId('blog-comment')
    //   .contains('Alex Lather ')
    //   ;
    // cy.getByTestId('blog-comment-content').type('{ selectall }');

    // cy.getByTestId('pagination-bottom').contains('следующая').click();
  });
});
