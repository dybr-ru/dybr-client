describe('User creates account', () => {
  const email = Cypress.env('username');
  const password = Cypress.env('password');
  const profileName = Cypress.env('profileName');
  const slug = Cypress.env('slug');
  const diaryName = Cypress.env('diaryName');

  // todo enable it back when switching back to first profile is implemented
  it.skip('User creates new community', function () {
    cy.visit(Cypress.env('baseUrl'));

    cy.get('[data-testid=login-form]');
    cy.get('#username').type(email);
    cy.get('#password').focus().type(password);
    cy.get('[data-testid=login-button]').click();

    cy.getByTestId('user-menu-button').click();
    cy.getByTestId('sidebar-create-new-profile-link').click();

    cy.get('[data-testid=new-profile-creation-header]');
    cy.get('[data-testid=new-profile-community-switcher]').click();

    cy.get('#nickname').type(profileName + ' community');
    cy.get('#title').type(diaryName + ' community');
    cy.get('#slug').type(slug + '_com');

    cy.get('[data-testid=create-new-profile-button]').click();

    cy.get('[data-testid=new-profile-created-success]');
    cy.get('[data-testid=navigate-to-main-button]').click();
    // todo switch back to main profile at the end
  });
});
