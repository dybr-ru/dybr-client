describe('User creates account', () => {
  const email = Cypress.env('username');
  const password = Cypress.env('password');
  const profileName = Cypress.env('profileName');
  const slug = Cypress.env('slug');
  const diaryName = Cypress.env('diaryName');

  it('User registers new account 1', function () {
    cy.visit(Cypress.env('baseUrl'));

    cy.get('[data-testid=create-account-button]').click();
    cy.get('#email').type(email);
    cy.get('#pass').focus().type(password);
    cy.get('#pass2').focus().type(password);

    cy.get('#tos-checkbox').click();
    cy.get('#adult-checkbox').click();

    cy.get('[data-testid=registration-form]').submit();

    cy.get('[data-testid=account-created-confirmation]');
  });
  it('User creates new profile', function () {
    cy.visit(Cypress.env('baseUrl'));

    cy.get('[data-testid=login-form]');
    cy.get('#username').type(email);
    cy.get('#password').focus().type(password);

    cy.get('[data-testid=login-button]').click();

    cy.get('[data-testid=new-profile-creation-header]');
    cy.get('[data-testid=new-profile-community-switcher]');

    cy.get('#nickname').type(profileName);
    cy.get('#title').type(diaryName);
    cy.get('#slug').type(slug);

    cy.get('[data-testid=create-new-profile-button]').click();

    cy.get('[data-testid=new-profile-created-success]');
    cy.get('[data-testid=navigate-to-main-button]').click();
  });
});
