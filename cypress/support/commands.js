// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
// -- This is a parent command --

Cypress.Commands.add('loginUI', () => {
  cy.visit(Cypress.env('baseUrl'));

  cy.get('[data-testid=login-form]');
  cy.get('#username').type('cypresstest@test.com');
  cy.get('#password')
    .focus()
    .type('test');
  cy.get('[data-testid=login-button]').click();
  cy.get('[data-testid=navbar-loggedin]');
  cy.get('[data-testid=salutation-section]');
});

Cypress.Commands.add('login', () => {
  const nonce = new Date().getTime();
  cy.request({
    method: 'POST',
    url: `${Cypress.env('host')}/sessions?nonce=${nonce}`,
    body: {
      data: {
        type: 'sessions',
        attributes: {
          action: 'login',
          email: Cypress.env('username'),
          password: Cypress.env('password')
        }
      }
    },
    headers: {
      'Content-Type': 'application/vnd.api+json'
    }
  }).then(res => {
    console.log('res.body.data: ', res.body.data);
    const storeState = {
      lists: {
        banned: { profiles: [] },
        hidden: { profiles: [] },
        favorites: [],
        readers: [],
        profiles: {}
      },
      user: {
        userData: {
          createdAt: '',
          updatedAt: '',
          email: '',
          isAdult: false,
          termsOfService: false
        },
        activeProfile: '0',
        userId: undefined,
        token: res.body.data.attributes['access-token'],
        settings: undefined,
        profiles: {}
      }
    };
    // doesn't work: gets overridden by the app
    window.localStorage.setItem(
      'persist:redux-store',
      JSON.stringify(storeState)
    );
  });
});

Cypress.Commands.add('getByTestId', (selector, ...args) => {
  return cy.get(`[data-testid=${selector}]`, ...args);
});
