- allows Extended Latin, Cyrillic, Hiragana, Katakana
- allowed symbols - \_ ~ ! @ # $ & \* ( ) + ? = / | . , ; : < > [ ] " '

- no mixing of cyrillics and latin
- no side spaces
- no double spaces

- min length 1
- max length 50

- backend: lowercase comparison for duplicates

** Allowed: **
Test
test
test test
тест
-\_~!@#$&\*()+?=/|.,;:<>[]"'
öäå
12345678901234567890123456789012345678901234567890

** Not allowed **
' test' (without ')
' test ' (without ')
12345678901234567890123456789012345678901234567890a
test тест
