On general Dybr Client Structure:

1.  All raster images should be in components/img/
2.  All vector images should be in components/Icons/
3.  All form elements should be in components/FormElements
4.  All items related to Froala text editor should be in components/Froala
5.  Logo should be in components/Logo
6.  All Dybr components should be in components/
    - Creating sub-folders is allowed (named after related Scene)
    - Components not included here: Header and Blog
    - Temporarily not included, but should be - SignUp
7.  Blog part is encapsulated and separate.

    - Uses CSS only and no styled components in order to support custom user styles.
    - All Blog components should be in scenes/Blog/components and contain no styles (!!!), structure only
    - All Blog styles in scenes/Blog/styles
    - Blog should not use any styled components or styles from elsewhere in the file tree.
    - Components and styles from scenes/Blog/ should not be used outside of Blog/

    - overlay components that are using the dybr scheme can and should use standard form elements. Examples: designer, dialog windows. These should not use any class names.
    - functional elements such as editor, More tags, etc. must be included from the /components.

8.  Header part is encapsulated and separate.
    - All Header components should be in scenes/Header/components
    - Header components may, if necessary, import components from /components
    - Components from scenes/Header/components should not be used outside of Header/

On logical structure:

1.  Scenes should not contain CSS styles, but structure only.

On style

1.  No use of 'rem' or 'em' in CSS. Pixel values preferred at this time. This may change later.
2.  Block Element Modifier methodology should be used
3.  Naming Conventions for Classes: TBD

Naming Conventions for Files:

1.  No spaces/underscrores/dashes
2.  Each word starts with a capital letter
3.  Styled components should have prefix "Styled"
4.  React components are assumed to have if there is no prefix

On main page structure:

```
<Whole Page Wrapper> /*defines styles for the whole page like font, etc.*/
    <Header /> /*includes navigation bar and sidebar*/
    <Page Wrapper> /*groups sections*/
        <Section> /*defined a horizontal section with or without background*/
            <Content Wrapper> /*defines width of the content,is displayed as flex, can have 1 or 2 child columns*/
                <Content Column> /*flex child with variable width*/
                    /*actual content goes here*/
                </Content Column>
            </Content Wrapper>
        </Section>
    </Page Wrapper>
    <Footer /> /*includes links to service pages*/
</Whole Page Wrapper>
```
