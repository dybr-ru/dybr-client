import { sentryVitePlugin } from '@sentry/vite-plugin';
import legacy from '@vitejs/plugin-legacy';
import react from '@vitejs/plugin-react';
import { resolve } from 'node:path';
import { visualizer } from 'rollup-plugin-visualizer';
import { defineConfig } from 'vite';
import svgr from 'vite-plugin-svgr';

export default defineConfig({
  plugins: [
    react(),
    svgr(),
    visualizer(),
    legacy({
      targets: [
        '>0.2%',
        'not dead',
        'last 2 versions',
        'Firefox ESR',
        'not op_mini all'
      ]
    }),
    sentryVitePlugin({
      org: 'dybrio',
      project: 'dybr-client'
    })
  ],
  resolve: {
    alias: [
      { find: '@', replacement: resolve(__dirname, './src') },
      {
        find: 'lodash',
        replacement: 'lodash-es'
      },
      {
        find: 'htmr',
        replacement: 'htmr/lib/htmr.js' // Load unminified htmr, because minified version fails on incorrect attributes, and we have A LOT of them. It's huge (200Kb instead of 2KB), but I have no other ideas.
        // TODO: Create ErrorBoundary for each entry to not crash the whole page
        // TODO: When sending user report, escape HTML characters in it ?
      }
    ]
  },
  server: {
    open: true,
    port: 3002
  },
  build: {
    sourcemap: true
  },
  optimizeDeps: {
    entries: []
  },
  define: {
    global: {}
  },
  test: {
    globals: true,
    environment: 'jsdom',
    root: 'src/',
    coverage: {
      exclude: [
        'src/styles/Icons/*.js',
        'src/utils/testUtils/*.js',
        'src/components/Froala/**/*.js',
        'src/components/Froala/*.js',
        'src/registerServiceWorker.js',
        'src/configs/*.js'
      ]
    }
  },
  base: '/'
});
