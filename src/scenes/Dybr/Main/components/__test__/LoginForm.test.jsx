import React from 'react';
import { generate } from '@/utils/testUtils/generator';
import 'styled-components';
import { LoginForm } from '../LoginForm';
import {
  cleanup,
  fireEvent,
  render,
  wait,
  waitForElement
} from '@testing-library/react';
import { useTokenState } from '@/store/localStorage/useTokenState';
import { vi, test, describe } from 'vitest';

// ensure you're resetting modules before each test
beforeEach(() => {
  vi.resetModules();
});

afterEach(cleanup);

/* does not work well with styled components
test('Match snapshot', () => {
  const { container } = render(<LoginForm user={{}} />);
  expect(container.firstChild).toMatchSnapshot();
});
*/
vi.mock('@/store/localStorage/useTokenState', () => {
  const mySetToken = vi.fn();

  return {
    useTokenState: () => ['123', mySetToken]
  };
});

test('Render what is supposed to be rendered ', () => {
  const { container, queryByTestId } = render(<LoginForm user={{}} />);
  expect(container.textContent).toContain('Вход на Дыбр');
  expect(container.textContent).toContain('почта');
  expect(container.textContent).toContain('пароль');
  expect(container.textContent).toContain('Войти');
  expect(container.textContent).toContain('Создать аккаунт');
  expect(container.textContent).toContain('Сменить пароль');

  const errorBlock = queryByTestId('error-block');
  expect(errorBlock.textContent).toBe('');
});

test('Allow user to type email and password in the fields', async () => {
  // Arrange
  const fakeUser = generate.loginForm();
  const { getByLabelText } = render(<LoginForm user={{}} />);
  const usernameNode = getByLabelText(/Почта/i);
  const passwordNode = getByLabelText(/Пароль/i);

  // Act
  fireEvent.change(usernameNode, { target: { value: fakeUser.username } });
  fireEvent.change(passwordNode, { target: { value: fakeUser.password } });

  expect(usernameNode.value).toBe(fakeUser.username);
  expect(passwordNode.value).toBe(fakeUser.password);
});

test('Call onSubmitLogin with username and passowrd when submitted', async () => {
  // Arrange
  const fakeUser = generate.loginForm();

  const doLogin = vi.fn(() => ({}));
  const { getByLabelText, getByText } = render(
    <LoginForm doLoginThunk={doLogin} />
  );

  const usernameNode = getByLabelText(/Почта/i);
  const passwordNode = getByLabelText(/Пароль/i);

  // Act

  fireEvent.change(usernameNode, { target: { value: fakeUser.username } });
  fireEvent.change(passwordNode, { target: { value: fakeUser.password } });

  getByText(/Войти/i).click();
  const [, setToken] = useTokenState();

  await wait(() => expect(doLogin).toHaveBeenCalledTimes(1));

  // Assert
  expect(doLogin).toHaveBeenCalledTimes(1);
  expect(doLogin).toHaveBeenCalledWith({
    email: fakeUser.username,
    password: fakeUser.password,
    setToken
  });
});

test('doLogin function is not called if email or password are empty, the errors are shown', async () => {
  // Arrange
  const doLogin = vi.fn();
  const { getByText } = render(<LoginForm doLoginThunk={doLogin} />);

  getByText(/Войти/i).click();

  await wait(() => expect(doLogin).toHaveBeenCalledTimes(0));
  const error = await waitForElement(() => getByText('не заполнено'));

  // Assert
  expect(doLogin).toHaveBeenCalledTimes(0);
  expect(error).toBeDefined();
});

test('Text field errors are shown after unsuccessful submit and removed on value change', () => {
  // Arrange
  const doLogin = vi.fn(() => ({ error: 'error' }));
  const { container, getByLabelText, getByText } = render(
    <LoginForm doLoginThunk={doLogin} />
  );
  // Act
  getByText(/Войти/i).click();
  const errorBefore = getByText('не заполнено');
  const usernameNode = getByLabelText(/Почта/i);
  const passwordNode = getByLabelText(/Пароль/i);
  fireEvent.change(usernameNode, { target: { value: '_' } });
  fireEvent.change(passwordNode, { target: { value: '_' } });
  getByText(/Войти/i).click();

  // Assert
  expect(doLogin).toHaveBeenCalledTimes(1);
  expect(errorBefore).toBeDefined();
  expect(container).not.toContain('не заполнено');
});

test('Submit error is shown', async () => {
  // Arrange
  const doLogin = vi.fn(() => ({ error: 'error' }));
  const { getByLabelText, getByText } = render(
    <LoginForm doLoginThunk={doLogin} />
  );
  // Act
  const usernameNode = getByLabelText(/Почта/i);
  const passwordNode = getByLabelText(/Пароль/i);
  fireEvent.change(usernameNode, { target: { value: '_' } });
  fireEvent.change(passwordNode, { target: { value: '_' } });
  getByText(/Войти/i).click();
  // Assert
  const errorNode = await waitForElement(() => getByText('error'));
  expect(errorNode).toBeDefined();
});

test('Show change password side if clicked "change password"', async () => {
  const { getByLabelText, getByText, queryByLabelText } = render(<LoginForm />);
  getByText(/Сменить пароль/i).click();
  const passwordNode = queryByLabelText(/пароль/i);
  const usernameNode = getByLabelText(/почта/i);

  expect(usernameNode).toBeDefined();
  expect(passwordNode).toBeNull();
});
test('Hide error block when sides are changed', async () => {
  const error = 'some error';
  const { container, getByText } = render(<LoginForm user={{ error }} />);

  // first, make the error show
  getByText(/Войти/i).click();

  getByText(/Сменить пароль/i).click();
  // const errorBlock = queryByTestId('error-block');

  // Assert
  expect(container).not.toContain(error);
});

test('Show error block when there is password change request error', async () => {
  const onResetPassword = vi.fn(() => ({ error: 'error' }));
  const { getByLabelText, getByText } = render(
    <LoginForm requestNewPasswordThunk={onResetPassword} />
  );
  const usernameNode = getByLabelText(/Почта/i);
  fireEvent.change(usernameNode, { target: { value: '_' } });
  getByText(/Сменить пароль/i).click();
  getByText(/Запросить смену пароля/i).click();

  // Assert
  const errorNode = await waitForElement(() => getByText('error'));
  expect(errorNode).toBeDefined();
});

test('Show email on change password side if it was filled already', async () => {
  const { getByLabelText, getByText } = render(<LoginForm />);

  const { username } = generate.loginForm();
  const usernameNode = getByLabelText(/Почта/i);
  fireEvent.change(usernameNode, { target: { value: username } });
  getByText(/Сменить пароль/i).click();
  const value = usernameNode.value;

  // Assert
  expect(value).toEqual(username);
});
test('Request password change function is called', async () => {
  // Arrange
  const { username } = generate.loginForm();
  const onResetPassword = vi.fn(() => ({}));
  const { getByLabelText, getByText, rerender } = render(
    <LoginForm requestNewPasswordThunk={onResetPassword} />
  );
  const usernameNode = getByLabelText(/Почта/i);

  // Act
  fireEvent.change(usernameNode, { target: { value: username } });
  getByText(/Сменить пароль/i).click();
  getByText(/Назад, я вспомнил/i);
  getByText(/Запросить смену пароля/i).click();

  await wait(() => expect(onResetPassword).toHaveBeenCalledTimes(1));

  rerender(<LoginForm />);
  getByText(/Ссылка отправлена на указанную почту!/i);
  // also check that afterwards there is button to go back and it works
  getByText(/Назад/i).click();
  getByText(/Сменить пароль/i);
});
test('Request password change function is not called if email is empty', async () => {
  // Arrange
  const requestPassword = vi.fn();
  const { getByText } = render(<LoginForm user={{ requestPassword }} />);

  getByText(/Сменить пароль/i).click();
  getByText(/Запросить смену пароля/i).click();

  const error = await waitForElement(() => getByText('не заполнено'));

  // Assert
  expect(requestPassword).toHaveBeenCalledTimes(0);
  expect(error).toBeDefined();
});

test("Show 'submitting...' when login info was sent", async () => {
  const fakeUser = generate.loginForm();
  const doLogin = vi.fn(() => Promise.resolve({}));

  const { getByLabelText, getByText } = render(
    <LoginForm doLoginThunk={doLogin} />
  );
  const usernameNode = getByLabelText(/Почта/i);
  const passwordNode = getByLabelText(/Пароль/i);
  // Act
  fireEvent.change(usernameNode, { target: { value: fakeUser.username } });
  fireEvent.change(passwordNode, { target: { value: fakeUser.password } });

  getByText(/Войти/i).click();
  const btnInProgress = getByText(/Входим.../i);
  expect(btnInProgress).toHaveProperty('disabled');
});
test("Don't show 'submitting...' when info was not accepted by the server", async () => {
  const fakeUser = generate.loginForm();
  const doLogin = vi.fn(() => ({ error: 'error' }));
  const { getByLabelText, getByText } = render(
    <LoginForm doLoginThunk={doLogin} />
  );
  const usernameNode = getByLabelText(/Почта/i);
  const passwordNode = getByLabelText(/Пароль/i);
  // Act
  fireEvent.change(usernameNode, { target: { value: fakeUser.username } });
  fireEvent.change(passwordNode, { target: { value: fakeUser.password } });

  getByText(/Войти/i).click();
  getByText(/Входим.../i);
  await wait(() => getByText(/Войти/i));
});
test("Don't show 'submitting...' when info was accepted by the server", async () => {
  const doLogin = vi.fn(() => ({ error: 'error' }));

  const { queryByText } = render(<LoginForm doLoginThunk={doLogin} />);
  expect(queryByText(/Входим.../i)).toBeNull();
});
