import React, { useState } from 'react';
import ContentWrap from '@/components/DybrComponents/ContentWrap';
import { ImportantButton } from '@/components/Shared/Buttons';

import {
  SectionNarrowBlock,
  SectionTitle
} from '@/components/DybrComponents/Section';

export function ProduceError() {
  const [error, setError] = useState(false);
  if (error) {
    throw new Error('Здесь будет error message');
  }

  return (
    <ContentWrap>
      <SectionTitle>
        На этой странице можно проверить обработку ошибок
      </SectionTitle>

      <SectionNarrowBlock size="800px">
        <div>
          <ImportantButton onClick={() => setError(true)}>
            Сгенерировать ошибку!
          </ImportantButton>
          <br />
        </div>
      </SectionNarrowBlock>
    </ContentWrap>
  );
}
