import React from 'react';
import {
  cleanup,
  render,
  wait,
  waitForElement,
  within
} from '@testing-library/react';
import { BlogFeed } from '../BlogFeed';
import { query as queryMock } from '@/api/Query';
import defaultSettings from '@/configs/defaultUserSettings';
import { generate } from '@/utils/testUtils/generator';
// import '@testing-library/jest-dom/extend-expect';
import { vi, test, describe } from 'vitest';

vi.mock('api/Query');
vi.mock('api/entries');

// TODO: provide context
describe('Blog Scene', () => {
  beforeEach(() => {
    vi.resetModules();
    vi.clearAllMocks();
  });
  afterEach(cleanup);

  it.skip('fetches entries', async () => {
    queryMock.mockImplementation(() => Promise.resolve({ data: {}, meta: {} }));

    const location = { search: '' };
    const { container, getByLabelText, getByText } = await render(
      <BlogFeed
        location={location}
        settings={defaultSettings}
        blog={{
          id: '1'
        }}
      />
    );

    expect(queryMock).toHaveBeenCalledTimes(1);
    expect(queryMock).toHaveBeenCalledWith({
      method: 'GET',
      resource: 'blogs/1/entries',
      queryParams: {
        'filters[state]': 'published',
        sort: '-published-at',
        'page[size]': defaultSettings.pagination.entries,
        'page[number]': 1,
        include: 'profile'
      }
    });
  });

  it.skip('fetches entries for the right page', async () => {
    queryMock.mockImplementation(() => Promise.resolve({ data: {}, meta: {} }));

    const location = { search: '?page=2' };
    const { container, getByLabelText, getByText } = await render(
      <BlogFeed
        location={location}
        settings={defaultSettings}
        blog={{
          id: '1'
        }}
      />
    );

    expect(queryMock).toHaveBeenCalledTimes(1);
    expect(queryMock).toHaveBeenCalledWith({
      method: 'GET',
      resource: 'blogs/1/entries',
      queryParams: {
        'filters[state]': 'published',
        sort: '-published-at',
        'page[size]': defaultSettings.pagination.entries,
        'page[number]': 2,
        include: 'profile'
      }
    });
  });

  it.skip('displays "no entries" if nothing is returned', async () => {
    queryMock.mockImplementation(() => Promise.resolve({ data: [], meta: {} }));

    const location = { search: '' };
    const { container, getByLabelText, getByText } = await render(
      <BlogFeed
        location={location}
        settings={defaultSettings}
        blog={{
          id: '1'
        }}
      />
    );
    // expect(queryMock).toHaveBeenCalledTimes(1);
    await waitForElement(() => getByText(/ничего нет/i));

    // expect(container).toMatchSnapshot();
  });

  it.skip('displays an entry', async () => {
    const entries = generate.entries(1);
    queryMock.mockImplementation(() =>
      Promise.resolve({ data: entries, meta: { totalFound: 1 } })
    );

    const location = { search: '' };
    const { container, getByLabelText, getByText } = await render(
      <BlogFeed
        key={1}
        location={location}
        settings={defaultSettings}
        blog={{
          id: '1'
        }}
      />
    );
    await waitForElement(() => getByText(entries[0].title), { timeout: 300 });
    // getByText(entries[0].content);
  });

  it.skip('displays pagination', async () => {
    const entries = generate.entries(10);
    queryMock.mockImplementation(() =>
      Promise.resolve({ data: entries, meta: { totalEntries: 12 } })
    );

    const location = { search: '' };
    const { container, getByLabelText, getByText, getByTestId } = await render(
      <BlogFeed
        key={1}
        location={location}
        settings={defaultSettings}
        blog={{
          id: '1',
          blogSlug: 'aaa'
        }}
      />
    );
    await waitForElement(() => getByText(/следующая/i));
    const pagination = within(getByTestId('pagination'));

    //expect(getByText('1')).toHaveAttribute('class', 'current');
    pagination.getByText('2');
  });
  it.skip('displays pagination last page', async () => {
    const entries = generate.entries(20);
    queryMock.mockImplementation(() =>
      Promise.resolve({ data: entries, meta: { totalEntries: 20 } })
    );

    const location = { search: '?page=2' };
    const { container, queryByText, getByLabelText, getByText } = await render(
      <BlogFeed
        key={1}
        location={location}
        settings={defaultSettings}
        blog={{
          id: '1'
        }}
      />
    );
    await waitForElement(() => getByText(/предыдущая/i), { timeout: 300 });
    expect(queryByText(/следующая/i)).not.toBeInTheDocument();
    getByText('1');
    getByText('2');
  });
  it.skip('does not display pagination if only one page', async () => {
    const entries = generate.entries(1);
    queryMock.mockImplementation(() =>
      Promise.resolve({ data: entries, meta: { totalEntries: 1 } })
    );

    const location = { search: '?page=2' };
    const {
      container,
      queryByText,
      getByLabelText,
      getByText,
      getByTestId,
      queryByTestId
    } = await render(
      <BlogFeed
        key={1}
        location={location}
        settings={defaultSettings}
        blog={{
          id: '1'
        }}
      />
    );
    await wait(() => expect(queryByTestId('pagination')).toBe(null));
  });

  it.skip('fetches entries for tags', async () => {
    queryMock.mockImplementation(() => Promise.resolve({ data: {}, meta: {} }));

    const location = { search: '?tags=hello' };
    const { container, getByLabelText, getByText } = await render(
      <BlogFeed
        location={location}
        settings={defaultSettings}
        blog={{
          id: '1'
        }}
      />
    );

    expect(queryMock).toHaveBeenCalledTimes(1);
    expect(queryMock).toHaveBeenCalledWith('GET', 'blogs/1/entries', {
      'filters[state]': 'published',
      sort: '-published-at',
      'page[size]': defaultSettings.pagination.entries,
      'page[number]': 1,
      include: 'profile',
      'filters[tag]': ['hello']
    });
  });
});
