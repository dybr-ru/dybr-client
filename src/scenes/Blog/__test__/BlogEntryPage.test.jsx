/**
 * gets profile as prop from the scene
 *
 * fetch entry
 * render entry
 * fetch comments according the the url and user settings
 * display pagination if there are more comments
 * do not display pagination if there aren't more comments
 * render comments
 * render comment form if commenting is allowed (prop)
 * scroll to the right comment if the url has the comment param
 * scroll to the right comment if a comment link was clicked in another comment (how to make a test for that???)
 * add author and/or quote to the entry form if a reply button is clicked
 * go to the blog page on successfil entry delete
 * on unsuccessful - show a message
 * on comment delete do not reload the page but remove the comment
 * - optional, later - fetch the next comment and add it
 * on comment publish re-fetch comments and scroll the new comment into view
 * - on comment edit change the comment to the comment form
 * - on successful change do not reload the page
 *
 * if no entry, show InfoBlock
 */
import '@/utils/testUtils/matchMedia.mock'; // Must be imported before the tested file
import React from 'react';
import {
  cleanup,
  render,
  waitForElement,
  within
} from '@testing-library/react';
import { BlogEntryPageContainer } from '@/scenes/Blog/BlogEntryPage/BlogEntryPage';
import defaultSettings from '@/configs/defaultUserSettings';
import { generate } from '@/utils/testUtils/generator';
// import '@testing-library/jest-dom/extend-expect';

// import { Provider } from 'unstated';
import { getCommentsApi } from '@/api/comments';

jest.mock('api/Query');
jest.mock('api/entries');
jest.mock('api/comments', () => ({
  getCommentsApi: jest.fn()
}));

// TODO: provide context
describe('Blog Scene', () => {
  beforeEach(() => {
    jest.resetModules();
    jest.clearAllMocks();
  });
  afterEach(cleanup);

  it.skip('fetches entry', async () => {
    const entry = generate.entry();

    // entryApiMock.get.mockImplementation(() =>
    //   Promise.resolve({
    //     data: {
    //       ...entry,
    //       meta: { comments: 0, commentIds: [] }
    //     }
    //   })
    // );

    const location = { search: '' };
    const { container, getByLabelText, getByText } = await render(
      <Provider>
        <BlogEntryPageContainer
          location={location}
          settings={defaultSettings}
          eid="111"
          blog={{
            id: '1'
          }}
        />
      </Provider>
    );

    // expect(entryApiMock.get).toHaveBeenCalledTimes(1);
    // expect(entryApiMock.get).toHaveBeenCalledWith('entries/111', {
    //   include: 'profile'
    // });
  });

  it.skip('renders entry', async () => {
    const entry = generate.entry();
    // entryApiMock.get.mockImplementation(() =>
    //   Promise.resolve({ data: { ...entry, meta: { comments: 12 } } })
    // );
    const location = { search: '' };
    const { getByText } = await render(
      <Provider>
        <BlogEntryPage
          key={1}
          location={location}
          settings={defaultSettings}
          blog={{
            id: '1'
          }}
        />
      </Provider>
    );
    await waitForElement(() => getByText(entry.title), { timeout: 300 });
    getByText(entry.content);
  });

  it.skip('fetches comments', async () => {
    const comments = generate.entries(1);
    const entry = generate.entry();
    // entryApiMock.get.mockImplementation(() =>
    //   Promise.resolve({
    //     data: {
    //       ...entry,
    //       meta: { comments: 1, commentIds: [1848] }
    //     }
    //   })
    // );
    getCommentsApi.mockImplementation(() =>
      Promise.resolve({ data: comments })
    );
    const location = { search: '' };
    const { getByText } = await render(
      <Provider>
        {' '}
        <BlogEntryPage
          key={1}
          location={location}
          settings={defaultSettings}
          blog={{
            id: '1'
          }}
        />
      </Provider>
    );
    await waitForElement(() => getByText(comments[0].content), {
      timeout: 300
    });
  });
  it.skip('displays pagination', async () => {
    const comments = generate.entries(15);
    const entry = generate.entry();
    // entryApiMock.get.mockImplementation(() =>
    //   Promise.resolve({
    //     data: {
    //       ...entry,
    //       meta: { comments: 15, commentIds: comments.map(c => c.id) }
    //     }
    //   })
    // );
    getCommentsApi.mockImplementation(() =>
      Promise.resolve({ data: comments.slice(0, 5) })
    );
    const location = { search: '' };
    const { getByTestId, getByText, queryByText } = await render(
      <Provider>
        <BlogEntryPage
          key={1}
          eid={entry.id}
          location={location}
          settings={{ pagination: { comments: 5 } }}
          blog={{
            id: '1',
            blogSlug: 'aaa'
          }}
        />
      </Provider>
    );
    await waitForElement(() => getByText(comments[0].content), {
      timeout: 300
    });
    expect(getCommentsApi).toHaveBeenCalledWith({
      entryId: entry.id,
      sort: 'created-at',
      'page[size]': 5,
      'page[number]': 1,
      include: 'profile'
    });
    expect(queryByText(comments[6].title)).not.toBeInTheDocument();
    await waitForElement(() => getByText(/следующая/i));
    const pagination = within(getByTestId('pagination'));

    expect(getByText('1')).toHaveAttribute('class', 'current');
    pagination.getByText('2');
  });

  it.skip('correctly chooses the page for linked comment', async () => {
    const comments = generate.entries(15).sort((a, b) => {
      return a.id > b.id ? 1 : -1;
    });
    const entry = generate.entry();
    // entryApiMock.get.mockImplementation(() =>
    //   Promise.resolve({
    //     data: {
    //       ...entry,
    //       meta: { comments: 15, commentIds: comments.map(c => c.id) }
    //     }
    //   })
    // );
    const commentID = comments[7].id;

    getCommentsApi.mockImplementation(() =>
      Promise.resolve({ data: comments.slice(0, 5) })
    );

    const location = { hash: '#' + commentID };
    const { getByText } = await render(
      <Provider>
        <BlogEntryPage
          key={1}
          eid={entry.id}
          location={location}
          settings={{ pagination: { comments: 5 } }}
          blog={{
            id: '1',
            blogSlug: 'aaa'
          }}
        />
      </Provider>
    );
    await waitForElement(() => getByText(comments[0].content), {
      timeout: 300
    });
    expect(getCommentsApi).toHaveBeenCalledWith({
      entryId: entry.id,
      sort: 'created-at',
      'page[size]': 5,
      'page[number]': 2,
      include: 'profile'
    });
  });
});
