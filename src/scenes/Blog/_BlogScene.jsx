import { useEffect, useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { getProfileByBlogSlug } from '@/api/profiles';
import { getDesignAPI } from '@/api/designs';

import NotFound from '@/scenes/Dybr/Static/NotFound';

import BlogHeader from '@/components/BlogComponents/BlogHeader';
import Footer from '@/scenes/Footer/Footer';
import BlogActionsSidePanel from '@/components/BlogComponents/BlogSidePanel';
import { DesignerPanelContainer } from './designer/_DesignerScene';
import Loading from '@/components/Shared/Loading';

import style from '@/styles/blog/BlogCssConstructor';
import composeBlogTheme from '@/styles/blog/ComposeBlogTheme';
import {
  Container,
  Content,
  ContentContainer,
  Header,
  Page
} from '@/styles/blog/BlogLayout/StyledBlocks';
import { ThemeProvider } from 'styled-components';
import { selectDraftDesign } from '@/store/designer/selectors';
import { updateProfileSetting } from '@/store/user/thunks/updateProfileSetting';
import { updateUserSetting } from '@/store/user/thunks/updateUserSettingThunk';
import { selectUserSettingsAugmented } from '@/store/user/selectors/selectUserSettingsAugmented';
import { selectProfileIsFavoriteBySlug } from '@/store/lists/selectors/selectProfileIsFavoriteBySlug';
import { BlogFeedContainer } from './BlogFeed';
import { BlogEntryEditContainer } from './EntryEdit/BlogEntryEdit';
import { BlogEntryPageContainer } from './BlogEntryPage/BlogEntryPage';
import { BlogFavoritesContainer } from './BlogFavorites';
import { getCommunitiesEntriesApi, getFavoriteEntriesApi } from '@/api/entries';
import { getBookmarksApi } from '@/api/bookmarks';
import css from './_BlogScene.module.css';
import {
  useActiveProfile,
  useTokenState
} from '@/store/localStorage/useTokenState';
import WithActiveProfileId from '@/store/localStorage/WithActiveProfileId';
import { useParams } from 'react-router-dom';

/**
 * Scenes are mainly for routing and getting the data used in all sub-pages or sections
 *
 * The blog scene needs to do the following:
 * + fetch blofile
 * + fetch blog design and compose the final theme, put the css on the page
 * + route to the right page
 *
 */

const isDesignDisabled = (userSettings, design) => {
  let disabled = userSettings.disabledDesigns;
  const id = design?.id;

  if (!id || id === '0') {
    return false;
  }
  return disabled.includes(id);
};

export function Blog({
  updateProfileSetting,
  designerOn,
  designerCollapsed,
  designDraft,
  userSettings,
  updateUserSetting,
  ...restProps
}) {
  const [loading, setLoading] = useState(true);
  const [blog, setBlog] = useState({});
  const [design, setDesign] = useState();
  const [designDisabled, setDesignDisabled] = useState(false);
  const [sidePanelSaving, setSidePanelSaving] = useState(false);
  const [error, setError] = useState(false);
  const { activeProfileId, userId } = useActiveProfile();
  const [token] = useTokenState();
  const loggedIn = Boolean(token);

  const { slug } = useParams();

  useEffect(() => {
    window.scrollTo(0, 0);
    const loadBlog = async () => {
      setLoading(true);
      if (!slug) {
        console.log('no slug');
        return;
      }

      const res = await getProfileByBlogSlug({ slug, token, withTags: true });
      const blogData = res.data;
      if (res.error) {
        setError(true);
      } else if (blogData) {
        setBlog(blogData);

        const designId = blogData.settings?.currentDesign;

        let newDesign = {};
        if (designId && designId !== '0') {
          const designRes = await getDesignAPI({
            designId,
            profileId: blogData.id,
            token
          });

          if (!designRes.error) {
            // NOTE: most designs have id in the attributes, but apparently some
            // older ones have "0", so make sure we use the actual id.
            // And honestly, id should not be there to begin with...
            newDesign = { ...designRes.data, id: designId };
            setDesign(newDesign);
          }
        } else {
          setDesign(newDesign);
        }
        setDesignDisabled(isDesignDisabled(userSettings, newDesign));
      }
      setLoading(false);
    };
    loadBlog();
  }, [activeProfileId, slug]);

  const hotUpdateTagList = tags => {
    const blogTags = [...(blog.tags ?? [])];
    tags.forEach(entryTag => {
      const tagIndex = blogTags.findIndex(t => t.name === entryTag);
      if (tagIndex !== -1) {
        blogTags[tagIndex] = {
          ...blogTags[tagIndex],
          entries: blogTags[tagIndex].entries + 1
        };
      } else {
        blogTags.push({ name: entryTag, entries: 1 });
      }
    });
    setBlog(prevState => ({ ...prevState, tags: blogTags }));
  };

  const updatePinnedInProfiles = pinned => {
    updateProfileSetting('pinnedEntries', pinned, token);
    setBlog(prevState => ({
      ...prevState,
      settings: { ...prevState.settings, pinnedEntries: pinned }
    }));
  };

  const pinEntry = async entryID => {
    const pinned = blog.settings.pinnedEntries;
    // todo only owner and admins in community should be allowed to do that
    if (pinned && pinned.length && !pinned.find(p => p === entryID)) {
      if (
        window.confirm(
          'Ранее закрепленная запись будет откреплена. Продолжить?'
        )
      ) {
        updatePinnedInProfiles([entryID]);
        return;
      } else return;
    }
    updatePinnedInProfiles([entryID]);
  };

  const unpinEntryIfPinned = entryID => {
    const pinned = blog.settings.pinnedEntries;

    if (pinned.find(e => e === entryID)) {
      updatePinnedInProfiles([]);
    }
  };

  const toggleDefaultStyle = async () => {
    if (sidePanelSaving) {
      return;
    }
    setSidePanelSaving(true);
    setDesignDisabled(prevState => !prevState);

    let disabled = [...userSettings.disabledDesigns];
    const id = design?.id;

    if (id && id !== '0' && !isDesignDisabled(userSettings, design)) {
      disabled.push(id);
    } else {
      disabled = disabled.filter(d => d !== id);
    }
    // this is cleanup of old disabled designs
    disabled = disabled.filter(d => d && d !== '0');

    const res = await updateUserSetting(
      'disabledDesigns',
      disabled,
      token,
      userId
    );

    if (!res) {
      setDesignDisabled(prevState => !prevState);
    }

    setSidePanelSaving(false);
  };

  if (loading) return <Loading />;

  if (!loading && error) {
    return (
      <div className={css.error}>
        Не удалось получить данные. Попробуйте перезагрузить страницу
      </div>
    );
  }

  const isOwnBlog = blog.id === activeProfileId;

  let theme = composeBlogTheme({
    loggedIn,
    ownBlog: isOwnBlog,
    tmpDesign: designDraft,
    designer: designerOn,
    blogDesignDisabled: designDisabled,
    blogTheme: design
  });

  return (
    <ThemeProvider theme={theme}>
      <Page>
        <style>{style(theme)}</style>

        <div data-testid="blog" className="blog-page">
          <Header className={'blog-header-container ' + theme.layout.align}>
            <Routes>
              <Route path=":eid" element={<BlogHeader profile={blog} />} />
              <Route path="favorites" element={<BlogHeader profile={blog} />} />
              <Route path="bookmarks" element={<BlogHeader profile={blog} />} />
              <Route
                path="communities"
                element={<BlogHeader profile={blog} />}
              />
              <Route path="*" element={<BlogHeader profile={blog} />} />
            </Routes>
          </Header>
          <Container
            className={'blog-container ' + theme.layout.align}
            designer={designerOn}
            designerPreviewMode={designerCollapsed}
          >
            <ContentContainer className="blog-content-container">
              <Content className="blog-content">
                <Routes>
                  <Route path="/" element={<BlogFeedContainer blog={blog} />} />
                  <Route
                    path=":eid"
                    element={<BlogEntryPageContainer blog={blog} />}
                  />
                  <Route
                    path=":eid/edit"
                    element={
                      <BlogEntryEditContainer
                        blog={blog}
                        hotUpdateTagList={hotUpdateTagList}
                        pinEntry={pinEntry}
                        unpinEntryIfPinned={unpinEntryIfPinned}
                      />
                    }
                  />
                  <Route
                    path="favorites"
                    element={
                      <BlogFavoritesContainer
                        ownerProfile={blog}
                        apiCallback={getFavoriteEntriesApi}
                      />
                    }
                  />
                  <Route
                    path="bookmarks"
                    element={
                      <BlogFavoritesContainer
                        ownerProfile={blog}
                        apiCallback={getBookmarksApi}
                      />
                    }
                  />
                  <Route
                    path="communities"
                    element={
                      <BlogFavoritesContainer
                        ownerProfile={blog}
                        apiCallback={getCommunitiesEntriesApi}
                      />
                    }
                  />
                  <Route
                    path="*"
                    element={
                      <NotFound
                        reason={'Blog: Не можем найти такую страницу'}
                      />
                    }
                  />
                </Routes>
              </Content>
            </ContentContainer>
          </Container>
        </div>
      </Page>
      {loggedIn && (
        <BlogActionsSidePanel
          profile={blog}
          isDefaultStyleOn={designDisabled}
          toggleDefaultStyle={toggleDefaultStyle}
          loading={sidePanelSaving}
          blogTheme={theme}
          higher={restProps['*'] === 'favorites'}
        />
      )}

      {isOwnBlog && designerOn && (
        <DesignerPanelContainer applyDesign={() => setDesign(designDraft)} />
      )}
      <Footer copyright={false} />
    </ThemeProvider>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    isFavorite: selectProfileIsFavoriteBySlug(state, ownProps.slug),
    designerOn: state.designer.designerOn,
    designerCollapsed: state.designer.designerCollapsed,
    designDraft: selectDraftDesign(state),
    userSettings: selectUserSettingsAugmented(state)
  };
};

const mapDispatchToProps = {
  updateProfileSetting,
  updateUserSetting
};

export const BlogContainer = WithActiveProfileId(
  connect(mapStateToProps, mapDispatchToProps)(Blog)
);
