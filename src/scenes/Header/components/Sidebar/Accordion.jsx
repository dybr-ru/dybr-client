import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import ArrowIconDown from '@/styles/Icons/SimpleArrowDownWithBackground';
import ArrowIconUp from '@/styles/Icons/SimpleArrowUpWithBackground';

import { StyledCommentCounter, StyledDiscussionSection } from './styled';

function Accordion({
  title,
  initialState,
  canChangeState = true,
  onClick,
  notificationsCounter,
  notificationsLink,
  notificationsTitle,
  children
}) {
  const [isOpen, setOpen] = useState(initialState === 'open');
  return (
    <StyledDiscussionSection>
      <div
        className={`section-header ${
          isOpen ? 'section-open' : 'section-closed'
        }`}
      >
        <div
          className="section-title"
          onClick={canChangeState ? () => setOpen(!isOpen) : onClick}
        >
          <div className="arrow">
            {canChangeState && (isOpen ? <ArrowIconUp /> : <ArrowIconDown />)}
          </div>
          {title}
        </div>
        {notificationsCounter > 0 && (
          <Link to={notificationsLink} title={notificationsTitle}>
            <StyledCommentCounter className="section-notification-count">
              {notificationsCounter}
            </StyledCommentCounter>
          </Link>
        )}
      </div>
      {isOpen && children}
    </StyledDiscussionSection>
  );
}

export default Accordion;
