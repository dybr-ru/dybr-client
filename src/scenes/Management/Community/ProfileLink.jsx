import { getAvatar } from '@/utils/helpers/profiles';
import React from 'react';
import css from '@/scenes/Management/Community/ProfileLink.module.css';
import { classNames } from '@/utils/helpers/classNames';
import { Link } from 'react-router-dom';

function ProfileLink({
  profile,
  className = css.profileName,
  link,
  children: payload
}) {
  const avatar = getAvatar(profile);
  const profileLink = link ?? `/profile/${profile.id}`;

  return (
    <div className={css.container}>
      <div className={css.avatar}>
        <img className={css.avatarImg} src={avatar} alt="" />
      </div>
      <div className={css.data}>
        <Link
          to={profileLink}
          className={classNames({
            [css.link]: true,
            [className]: Boolean(className)
          })}
        >
          {profile.nickname}
        </Link>
        <>{payload}</>
      </div>
    </div>
  );
}
export default ProfileLink;
