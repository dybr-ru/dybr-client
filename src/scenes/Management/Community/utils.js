export function communityHasMemberModeration(settings) {
  return (settings.community?.joinRequest ?? 'approve') === 'approve';
}
