import packageJson from '../../../package.json';

export function getDybrVersion() {
  return packageJson.version;
}
