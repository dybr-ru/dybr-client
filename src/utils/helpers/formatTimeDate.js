import moment from 'moment';

export function formatTimeDate(date) {
  return moment(date).format('HH:mm DD.MM.YYYY');
}

export function formatDateTime(date) {
  return moment(date).format('DD.MM.YYYY HH:mm');
}
