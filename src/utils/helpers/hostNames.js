import { IMG_HOSTNAME } from '@/configs/dybr';

export function getRelativeUrl(url) {
  // TODO: debug why there's sometimes an object coming for background url
  if (typeof url !== 'string') return '';
  const devPrefix = IMG_HOSTNAME === undefined ? '' : IMG_HOSTNAME;
  return devPrefix + url.replace('https://dybr.ru', '');
}
