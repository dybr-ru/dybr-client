// in Froala it is close to impossible to correctly wrap mixes of paragraphs and MORE tags, so BRs are used instead.
// but for display we want paragraphs, which allows better styling and copying of the text.

export default function brToParagraphs(html = '') {
  html = '<p>' + html.replace(/<br><br>/g, '</p><p>') + '</p>';
  return html;
}
