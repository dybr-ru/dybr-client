import { describe, expect, it } from 'vitest';
import {
  calculateReadAccessSettings,
  getCurrentAccessType,
  getCustomListReadAccess,
  getInternalAccessSettings
} from '@/utils/privacy/accessCalculation';
import { AccessType } from '@/utils/privacy/types/AccessType';

describe('Privacy access calculation: getInternalAccessSettings', () => {
  it('allow list access', async () => {
    const accessSettings = [{ type: 'private' }, { id: '2', type: 'list' }];

    const internalSettings = getInternalAccessSettings(accessSettings);

    expect(internalSettings).toEqual({
      accessType: 'private',
      denyCustomList: [],
      allowCustomList: ['2']
    });
  });
  it('deny list access', async () => {
    const accessSettings = [
      { type: 'registered' },
      { id: '2', type: 'list', deny: true }
    ];

    const internalSettings = getInternalAccessSettings(accessSettings);

    expect(internalSettings).toEqual({
      accessType: 'registered',
      denyCustomList: ['2'],
      allowCustomList: []
    });
  });
  it('favorites access', async () => {
    const accessSettings = [{ type: 'favorites' }];

    const internalSettings = getInternalAccessSettings(accessSettings);

    expect(internalSettings).toEqual({
      accessType: 'favorites',
      denyCustomList: [],
      allowCustomList: []
    });
  });
  it('empty access', async () => {
    const accessSettings = undefined;

    const internalSettings = getInternalAccessSettings(accessSettings);

    expect(internalSettings).toEqual({
      accessType: undefined,
      denyCustomList: [],
      allowCustomList: []
    });
  });
});

describe('Privacy access calculation: getCurrentAccessType', () => {
  it('allow list access', async () => {
    const settings = {
      permissions: {
        access: [
          { id: '3', type: 'list' },
          { type: 'private' },
          { id: '2', type: 'list' }
        ]
      }
    };

    const accessType = getCurrentAccessType(settings);

    expect(accessType).toEqual(AccessType.allowCustomList);
  });
  it('deny list access', async () => {
    const settings = {
      permissions: {
        access: [
          { type: 'registered' },
          { id: '2', type: 'list', deny: true },
          { id: '3', type: 'list', deny: true }
        ]
      }
    };

    const accessType = getCurrentAccessType(settings);

    expect(accessType).toEqual(AccessType.denyCustomList);
  });
  it('favorites access', async () => {
    const settings = {
      permissions: {
        access: [{ type: 'favorites' }]
      }
    };

    const accessType = getCurrentAccessType(settings);

    expect(accessType).toEqual(AccessType.favorites);
  });
  it('incorrect type with deny list access will still give deny type', async () => {
    const settings = {
      permissions: {
        access: [
          { type: 'favorites' },
          { id: '2', type: 'list', deny: true },
          { id: '3', type: 'list', deny: true }
        ]
      }
    };

    const accessType = getCurrentAccessType(settings);

    expect(accessType).toEqual(AccessType.denyCustomList);
  });

  it('mixed custom list types will still give deny type', async () => {
    const settings = {
      permissions: {
        access: [
          { type: 'favorites' },
          { id: '2', type: 'list' },
          { id: '3', type: 'list', deny: true }
        ]
      }
    };

    const accessType = getCurrentAccessType(settings);

    expect(accessType).toEqual(AccessType.denyCustomList);
  });
  it('incorrect type with allow list access will give selected type', async () => {
    const settings = {
      permissions: {
        access: [
          { type: 'favorites' },
          { id: '2', type: 'list' },
          { id: '3', type: 'list' }
        ]
      }
    };

    const accessType = getCurrentAccessType(settings);

    expect(accessType).toEqual(AccessType.favorites);
  });
});

describe('Privacy access calculation: calculateReadAccessSettings', () => {
  it('allow list access ', async () => {
    const settings = {
      permissions: {
        access: [
          { id: '3', type: 'list' },
          { type: 'private' },
          { id: '2', type: 'list' }
        ]
      }
    };

    const newReadAccess = calculateReadAccessSettings(
      { type: AccessType.allowCustomList },
      settings
    );

    expect(newReadAccess).toEqual([
      { type: 'private' },
      { id: '3', type: 'list' },
      { id: '2', type: 'list' }
    ]);
  });
  it('deny list access', async () => {
    const settings = {
      permissions: {
        access: [
          { type: 'registered' },
          { id: '2', type: 'list', deny: true },
          { id: '3', type: 'list', deny: true }
        ]
      }
    };

    const newReadAccess = calculateReadAccessSettings(
      { type: AccessType.denyCustomList },
      settings
    );

    expect(newReadAccess).toEqual([
      { type: 'registered' },
      { id: '2', type: 'list', deny: true },
      { id: '3', type: 'list', deny: true }
    ]);
  });
  it('set allow custom list access', async () => {
    const settings = {
      permissions: {
        access: [{ type: 'favorites' }]
      }
    };

    const newReadAccess = calculateReadAccessSettings(
      { type: AccessType.allowCustomList },
      settings
    );

    expect(newReadAccess).toEqual([{ type: 'private' }]);
  });
  it('set deny custom list access', async () => {
    const settings = {
      permissions: {
        access: [{ type: 'favorites' }]
      }
    };

    const newReadAccess = calculateReadAccessSettings(
      { type: AccessType.denyCustomList },
      settings
    );

    expect(newReadAccess).toEqual([{ type: 'registered' }]);
  });
});

describe('Privacy access calculation: getCustomListReadAccess', () => {
  it('add list to allow list access ', async () => {
    const newReadAccess = getCustomListReadAccess(AccessType.allowCustomList, [
      { value: '1' },
      { value: '2' },
      { value: '3' }
    ]);

    expect(newReadAccess).toEqual([
      { type: 'private' },
      { id: '1', type: 'list' },
      { id: '2', type: 'list' },
      { id: '3', type: 'list' }
    ]);
  });
  it('add list to deny list access', async () => {
    const newReadAccess = getCustomListReadAccess(AccessType.denyCustomList, [
      { value: '1' },
      { value: '2' },
      { value: '3' }
    ]);

    expect(newReadAccess).toEqual([
      { type: 'registered' },
      { id: '1', type: 'list', deny: true },
      { id: '2', type: 'list', deny: true },
      { id: '3', type: 'list', deny: true }
    ]);
  });
});
