import { faker } from '@faker-js/faker';

function userData(overrides = {}) {
  const password = overrides.password || faker.internet.password();
  return Object.assign({
    username: faker.internet.userName(),
    email: faker.internet.email(),
    password,
    token: faker.string.uuid(),
    passwordToken: faker.string.uuid()
  });
}

function entry(overrides) {
  return Object.assign(
    {
      title: faker.lorem.words(),
      // paragraphs return text with \n\r for newlines
      // jsdom doesn't like \r very much I think...
      content: faker.lorem.words(), //faker.lorem.paragraphs().replace(/\r/g, ''),
      /* tags: [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()].filter(
        (w, i, a) => a.indexOf(w) === i
      ),*/
      createdAt: faker.date.past().toJSON(),
      state: 'published',
      profile: {
        blogSlug: faker.string.uuid(),
        blogTitle: faker.lorem.words(),
        nickname: faker.lorem.words(),
        settings: {}
      },
      id: faker.datatype.number(100000),
      meta: {
        commenters: 2,
        comments: 2
      }
    },
    overrides
  );
}

function entries(n) {
  const entries = [];
  for (let i = 0; i < n; i++) {
    entries.push(entry());
  }
  return entries;
}

function loginForm(overrides) {
  return Object.assign(
    {
      username: faker.internet.email(),
      password: faker.internet.password()
    },
    overrides
  );
}

function notifications(comments, discussions) {
  const data = [];
  for (let i = 0; i < comments; i++) {
    data.push({
      blog: '1',
      comment: faker.datatype.number(100000),
      entry: '1',
      state: 'new',
      id: faker.datatype.number(100000)
    });
  }
  for (let i = 0; i < discussions; i++) {
    data.push({
      blog: faker.datatype.number(10),
      comment: faker.datatype.number(100000),
      entry: i + 5 + '',
      state: 'new',
      id: faker.datatype.number(100000)
    });
  }
  return data;
}

export const generate = {
  userData,
  entry,
  entries,
  loginForm,
  password: faker.internet.password,
  username: faker.internet.userName,
  id: faker.string.uuid,
  title: faker.lorem.words,
  content: faker.lorem.words,
  notifications
};
