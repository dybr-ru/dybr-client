import { vi } from 'vitest';

vi.mock('matchMedia', () => ({
  matches: false,
  media: query,
  onchange: null,
  addListener: vi.fn(),
  removeListener: vi.fn(),
  addEventListener: vi.fn(),
  removeEventListener: vi.fn(),
  dispatchEvent: vi.fn()
}));

// Add this line to define the window object
global.window = { matchMedia: vi.fn(() => ({ matches: false })) };
