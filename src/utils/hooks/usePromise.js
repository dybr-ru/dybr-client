import { useEffect, useState } from 'react';

/** just an idea, not implemented yet */

export default function usePromise(fn, watch = []) {
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState(null);
  const [error, setError] = useState(null);

  const refresh = async () => {
    setLoading(true);
    setError(null);
    try {
      const res = await fn();
      setResult(res);
    } catch (err) {
      setError(err);
    } finally {
      setLoading(false);
    }
  };

  useEffect(
    () => {
      refresh();
    },
    [...watch, refresh]
  );

  return { loading, result, error, refresh };
}
