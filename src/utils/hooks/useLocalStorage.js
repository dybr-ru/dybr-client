import React, { useEffect, useState } from 'react';

export default function useLocalStorage(key, defaultValue) {
  const [state, setState] = useState(() => {
    let value;
    try {
      value = JSON.parse(window.localStorage.getItem(key));
    } catch (e) {
      value = defaultValue;
    }
    return value;
  });

  useEffect(
    () => {
      window.localStorage.setItem(key, state);
    },
    [key, state]
  );

  function removeValue() {
    window.localStorage.removeItem(key);
  }

  return { state, setState, removeValue };
}
