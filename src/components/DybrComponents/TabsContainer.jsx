import React from 'react';
import css from '@/components/DybrComponents/TabsContainer.module.css';

const TabsContainer = (props) => (
  <div className={css.container}> {props.children}</div>
);

export default TabsContainer;
