import React from 'react';
import styled from 'styled-components';

const Header = styled.div`
  background: ${p => p.theme.backgroundLighter};

  h1 {
    text-transform: uppercase;
    font-size: 28px;
    font-family: ${p => p.theme.headerFont};
    width: 100%;
    text-align: center;
    padding: 82px 10px 0 10px;
    color: ${p => p.theme.text};

    @media (max-width: 600px) {
      padding: 20px 10px;
    }
  }
`;

const PageHeader = ({ children }) => {
  return (
    <Header>
      <h1>{children}</h1>
    </Header>
  );
};

export default PageHeader;
