import React from 'react';
import Tabs from './Tabs';
import { HeaderGeneral } from '@/components/DybrComponents/HeaderGeneral';

// todo deprecated in favor of Reach router Links, see FeedScenes for example
function HeaderWithTabs({ currentTab, tabs, onChange, name }) {
  return (
    <HeaderGeneral paddingBottom="45px">
      <h1>{name}</h1>
      <Tabs
        containerClassName="blog-header-tabs"
        currentTab={currentTab}
        tabs={tabs}
        onTabChange={onChange}
      />
    </HeaderGeneral>
  );
}

export default HeaderWithTabs;
