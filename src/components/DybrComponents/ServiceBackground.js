import styled from 'styled-components';
import bgTile from '@/styles/img/bg_tile.png';

export const ServiceBackground = styled.div`
  background-image: url(${bgTile});
  background-repeat: tile;
  min-height: calc(100vh - 50px);
`;
