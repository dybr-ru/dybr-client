import styled from 'styled-components';
// TODO: remove, they are not universal

const RequiredSymbol = styled.span`
  font-size: 28px;
  color: ${p => (p.error ? p.theme.brandDark : p.theme.accent)};
  margin-right: 5px;
`;

export default RequiredSymbol;
