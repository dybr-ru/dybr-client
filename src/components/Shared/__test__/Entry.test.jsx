// import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { render } from '@testing-library/react';
import Entry from '@/components/Shared/Entry/Entry';

const happyPath = {
  canManipulate: true,
  hasUserFunctions: true,
  type: 'feed-entry',
  entry: {
    title: 'Test title',
    content: 'Test Content!',
    createdAt: '2018-11-27T13:19:17.936347Z',
    id: '2120',
    meta: {
      commentIds: ['1111', '1123', '1125'],
      commenters: 2,
      comments: 3
    },
    profile: {
      blogSlug: 'test-blog',
      blogTitle: 'Test Blog',
      createdAt: '2018-09-10T18:35:07Z',
      id: '1',
      nickname: 'Test User',
      settings: {
        avatar: 'https://dybr.ru/img/3/1532725652_79066751.jpg',
        currentDesign: '1',
        subtext: 'I am some test text to appear under the nickname'
      }
    },
    settings: {
      feedExclude: false,
      permissions: {}
    },
    state: 'published',
    tags: ['tag 1', 'very important and long tag!!'],
    updatedAt: '2018-11-27T13:19:17.936347Z'
  }
  //handleReply: ƒ ()
  //onDelete: ƒ ()
};

// TODO: provide context
test.skip('renders an entry without crashing', () => {
  const { queryByText } = render(<Entry {...happyPath} />);

  expect(queryByText(happyPath.entry.title)).not.toBeNull();
  expect(queryByText('#' + happyPath.entry.tags[0])).not.toBeNull();
  expect(queryByText('#' + happyPath.entry.tags[1])).not.toBeNull();
});

test.skip('does not crash if entry has no setting object', () => {
  const { settings, ...incompleteEntry } = happyPath.entry;

  const { queryByText } = render(
    <Entry {...happyPath} entry={incompleteEntry} />
  );

  expect(queryByText(happyPath.entry.title)).not.toBeNull();
});

test.skip('renders lock icon if entry is private', () => {
  const closedEntry = {
    ...happyPath.entry,
    settings: {
      feedExclude: false,
      permissions: { access: [{ type: 'private' }] }
    }
  };
  const { getByTestId } = render(<Entry {...happyPath} entry={closedEntry} />);
  const lock = getByTestId('lock-icon-testid');
  expect(lock).toHaveAttribute('title', 'закрыта от всех');
});

test.skip('shows correct title for "open for favorites option"', () => {
  const closedEntry = {
    ...happyPath.entry,
    settings: {
      feedExclude: false,
      permissions: { access: [{ type: 'favorites' }] }
    }
  };
  const { getByTestId } = render(<Entry {...happyPath} entry={closedEntry} />);
  const lock = getByTestId('lock-icon-testid');
  expect(lock).toHaveAttribute('title', 'открыта для избранных');
});

test.skip('does not render the icon for closed for registered', () => {
  const notReallyClosed = {
    ...happyPath.entry,
    settings: {
      feedExclude: false,
      permissions: { access: [{ type: 'registered' }] }
    }
  };
  const { queryByTestId } = render(
    <Entry {...happyPath} entry={notReallyClosed} />
  );
  expect(queryByTestId('lock-icon-testid')).toBeNull();
});
