import { Routes, Route } from 'react-router-dom';
import { HeaderContainer } from '@/scenes/Header/_HeaderScene';
import React from 'react';
import { AppWrapper } from '@/App';
import ErrorLog from '@/scenes/Dybr/Static/ErrorLog';

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: 'errorboundary',
      title: 'Что-то пошло не так!',
      conjunction: 'но',
      hasError: false,
      stepsToReproduce: '',
      error: undefined,
      errorInfo: undefined
    };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, error: error };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error, errorInfo });
  }

  resetError = () => {
    this.setState({ hasError: false });
    this.setState({ error: undefined });
  }

  render() {
    if (this.state.hasError) {
      return (
        <AppWrapper>
          <Routes>
            <Route path="*" element={<HeaderContainer />} />
          </Routes>
          <ErrorLog
            mode="errorboundary"
            conjunction="но"
            title="Что-то пошло не так!"
            error={this.state.error}
            errorInfo={this.state.errorInfo}
            resetError={this.resetError}
          />
        </AppWrapper>
      );
    }

    return this.props.children;
  }
}