/**
 * shows on the blog pages in case no entries are found or there was some other problem with fetching data
 *
 * it is a container
 * accepts and renders children
 */
