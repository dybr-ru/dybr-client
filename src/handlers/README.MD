## Application logic layer.

### Responsible for:

- Bootstrapping the application
- Handling application events
- Handling auto-updates and sockets
- Calling api through api layer
- Updating global store
- Returning data back to UI

Currently this stuff is in the Context, mixed together with store. Will be moved here.
