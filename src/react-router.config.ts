import { type Config } from '@react-router/dev/config';

export default {
  ssr: false,
  prerender: [
    '/help',
    '/terms',
    '/terms/forbidden-actions',
    '/about',
    '/contacts'
  ]
} satisfies Config;
