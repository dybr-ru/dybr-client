require('jest-localstorage-mock');

import 'jest-enzyme';
import { JSDOM } from 'jsdom';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

const dom = new JSDOM();
global.document = dom.window.document;
global.window = dom.window;
global.window.scrollTo = () => {};

global.navigator = {
  userAgent: 'react.js'
};
