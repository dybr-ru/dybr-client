// import '@/utils/testUtils/matchMedia.mock'; // Must be imported before the tested file
// import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { render } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import theme from '@/styles/DybrTheme';
import { App } from '@/App';
// import { Provider } from 'unstated';
import { vi, test, describe } from 'vitest';
import ErrorBoundary from '@/components/Helpers/ErrorBoundary';

// global.$ = {};

vi.mock('@/store/localStorage/useTokenState');
// vi.stubGlobal('location', {
//   origin: 'https://example.com',
// });
// vi.stubGlobal('$', {});

describe('App', () => {
  test('app renders without crashing', () => {
    const { queryByText, container } = render(
      <ThemeProvider theme={theme}>
        <ErrorBoundary>
          <Provider>
            {(value) => <App store={value} />}
          <App store={{}} />

          <div id="modal-root" />
          </Provider>
        </ErrorBoundary>
      </ThemeProvider>
    );

    expect(container).toMatchSnapshot();
  });
})

