export class StoreEntriesAction {
  static type = 'notifications/STORE_ENTRIES';
  constructor(entries) {
    this.entries = entries;
    this.type = StoreEntriesAction.type;
  }
}
