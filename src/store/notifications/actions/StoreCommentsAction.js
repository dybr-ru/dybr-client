export class StoreCommentsAction {
  static type = 'notifications/STORE_COMMENTS';
  constructor(comments) {
    this.comments = comments;
    this.type = StoreCommentsAction.type;
  }
}
