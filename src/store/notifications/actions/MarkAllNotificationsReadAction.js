export class MarkAllNotificationsReadAction {
  static type = 'notifications/MARK_ALL_NOTIFICATIONS_READ';
  constructor() {
    this.type = MarkAllNotificationsReadAction.type;
  }
}
