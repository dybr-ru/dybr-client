export class ClearNotificationsAction {
  static type = 'notifications/CLEAR_NOTIFICATIONS';
  constructor() {
    this.type = ClearNotificationsAction.type;
  }
}
