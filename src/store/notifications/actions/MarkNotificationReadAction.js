export class MarkNotificationReadAction {
  static type = 'notifications/MARK_NOTIFICATION_READ';
  constructor(notificationId) {
    this.notificationId = notificationId;
    this.type = MarkNotificationReadAction.type;
  }
}
