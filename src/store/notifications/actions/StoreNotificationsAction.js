export class StoreNotificationsAction {
  static type = 'notifications/STORE_NOTIFICATIONS';
  constructor(items) {
    this.items = items;
    this.type = StoreNotificationsAction.type;
  }
}
