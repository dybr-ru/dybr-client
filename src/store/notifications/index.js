import { notificationsReducer } from './reducer';

import * as notificationsOperations from './operations';
import * as notificationsSelectors from './selectors';

export { notificationsOperations, notificationsSelectors };

export default notificationsReducer;
