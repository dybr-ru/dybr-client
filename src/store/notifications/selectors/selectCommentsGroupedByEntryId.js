import { createSelector } from 'reselect';
import { selectNotificationItems } from './selectNotificationItems';

/**
 * will return an object:
 * { 'entryID': [comment, comment] }
 */
export const selectCommentsGroupedByEntryId = createSelector(
  selectNotificationItems,
  notifications =>
    notifications.reduce((grouped, notification) => {
      if (notification.comments && notification.type === 'comment') {
        const comment = {
          ...notification.comments,
          profile: notification.profiles
        };
        if (notification.entry in grouped) {
          grouped[notification.entry].push(comment);
        } else {
          grouped[notification.entry] = [comment];
        }
      }
      return grouped;
    }, {})
);
