import { it, expect } from 'vitest';
import { initialState } from '@/store/_setup/initialState';
import { notificationItems } from './_utils';
import { selectNotificationsOwnBlog } from '@/store/notifications/selectors/selectNotifications';

it('selects notifications from own blog', () => {
  let state = initialState;

  state.notifications.items = notificationItems;

  expect(selectNotificationsOwnBlog(state, { activeProfileId: '666' })).toEqual(
    [
      {
        id: '1',
        blog: '666',
        comment: '12300',
        entry: '123',
        state: 'new',
        type: 'comment',
        comments: {
          title: 'hi'
        },
        profiles: {
          id: '6575'
        }
      },
      {
        id: '2',
        blog: '666',
        comment: '12301',
        entry: '123',
        state: 'new',
        type: 'comment',
        comments: {
          title: 'hello'
        },
        profiles: {
          id: '6574'
        }
      }
    ]
  );
});
