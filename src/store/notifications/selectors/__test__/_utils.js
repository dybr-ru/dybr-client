export const notificationItems = {
  1: {
    id: '1',
    blog: '666',
    comment: '12300',
    entry: '123',
    state: 'new',
    type: 'comment',
    comments: {
      title: 'hi'
    },
    profiles: {
      id: '6575'
    }
  },
  2: {
    id: '2',
    blog: '666',
    comment: '12301',
    entry: '123',
    state: 'new',
    type: 'comment',
    comments: {
      title: 'hello'
    },
    profiles: {
      id: '6574'
    }
  },
  3: {
    id: '3',
    blog: '777',
    comment: '12302',
    entry: '456',
    state: 'new',
    type: 'comment',
    comments: {
      title: 'ok'
    },
    profiles: {
      id: '6574'
    }
  }
};

export const notificationEntries = {
  123: {
    type: 'entries',
    id: '123',
    content: 'hello',
    settings: {},
    state: 'published',
    title: 'hi',
    profile: { id: '666' }
  },
  456: {
    type: 'entries',
    id: '456',
    content: 'hello',
    settings: {},
    state: 'published',
    title: 'hi',
    profile: { id: '777' }
  }
};
