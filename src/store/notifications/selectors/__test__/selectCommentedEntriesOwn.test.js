import { it, expect } from 'vitest';
import { initialState } from '@/store/_setup/initialState';
import { notificationItems } from './_utils';
import { selectCommentedEntriesOwn } from '@/store/notifications/selectors/selectNotificationEntries';
import { notificationEntries } from '@/store/notifications/selectors/__test__/_utils';

it('selects commented entries from own blog', () => {
  let state = initialState;

  state.notifications.items = notificationItems;
  state.notifications.entries = notificationEntries;

  expect(selectCommentedEntriesOwn(state, { activeProfileId: '666' })).toEqual([
    {
      type: 'entries',
      id: '123',
      content: 'hello',
      settings: {},
      state: 'published',
      title: 'hi',
      profile: { id: '666' }
    }
  ]);
});
