import { it, expect } from 'vitest';
import { selectNotificationEntryIDs } from '../index';

import { initialState } from '@/store/_setup/initialState';
import { notificationItems } from './_utils';

it('selects comment IDs, grouped by entry ID', () => {
  let state = initialState;

  state.notifications.items = notificationItems;

  expect(selectNotificationEntryIDs(state)).toEqual(['123', '456']);
});
