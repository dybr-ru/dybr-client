import { it, expect } from 'vitest';
import { selectNotificationsTotal } from '../index';

import { initialState } from '@/store/_setup/initialState';
import { notificationItems } from './_utils';

it('selects total amount of notifications', () => {
  let state = initialState;
  state.notifications.items = notificationItems;

  expect(selectNotificationsTotal(state)).toEqual(3);
});
