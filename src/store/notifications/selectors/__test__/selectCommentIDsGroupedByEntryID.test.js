import { it, expect } from 'vitest';
import { initialState } from '@/store/_setup/initialState';
import { notificationItems } from './_utils';
import { selectCommentsGroupedByEntryId } from '@/store/notifications/selectors/selectCommentsGroupedByEntryId';

it('selects comment IDs, grouped by entry ID', () => {
  let state = initialState;

  state.notifications.items = notificationItems;

  expect(selectCommentsGroupedByEntryId(state)).toEqual({
    '123': [
      {
        profile: {
          id: '6575'
        },
        title: 'hi'
      },
      {
        profile: {
          id: '6574'
        },
        title: 'hello'
      }
    ],
    '456': [
      {
        profile: {
          id: '6574'
        },
        title: 'ok'
      }
    ]
  });
});
