import { createSelector } from 'reselect';
import { selectNotificationItems } from './selectNotificationItems';

/**
 * will return an object:
 * { 'entryID': [comment, comment] }
 */
export const selectCommentsWithMentionGroupedByEntryId = createSelector(
  selectNotificationItems,
  items =>
    items.reduce((grouped, item) => {
      if (item.comments && item.type === 'mention') {
        const comment = {
          ...item.comments,
          profile: item.profiles
        };
        if (item.entry in grouped) {
          grouped[item.entry].push(comment);
        } else {
          grouped[item.entry] = [comment];
        }
      }
      return grouped;
    }, {})
);
