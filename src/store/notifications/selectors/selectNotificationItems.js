import { createSelector } from 'reselect';
import { selectNotificationsMap } from '@/store/notifications/selectors/selectNotificationsMap';

export const selectNotificationItems = createSelector(
  [selectNotificationsMap],
  items => Object.values(items)
);
