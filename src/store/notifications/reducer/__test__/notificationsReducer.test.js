import { describe, expect, test, it } from "vitest";
import { getEntries, getNotifications } from './payloadMocks';

import { defaultNotificationState } from '@/store/_setup/initialState';

import { StoreNotificationsAction } from '@/store/notifications/actions/StoreNotificationsAction';
import { StoreEntriesAction } from '@/store/notifications/actions/StoreEntriesAction';
import { notificationsReducer } from '@/store/notifications/reducer/notificationsReducer';

describe('notifications reducer', () => {
  it('returns given state on unknown action', () => {
    const state = { whatever: [{ listID: 1, data: 'some data' }] };
    const newState = notificationsReducer(state, {
      type: 'some_action',
      payload: { some: 'data' }
    });
    expect(newState).toEqual(state);
  });

  it('on StoreNotificationsAction, adds notifications object', () => {
    const notifications = getNotifications(2);

    const newState = notificationsReducer(
      defaultNotificationState,
      new StoreNotificationsAction(notifications)
    );
    expect(newState).toEqual({
      ...defaultNotificationState,
      items: {
        [notifications[0].id]: notifications[0],
        [notifications[1].id]: notifications[1]
      }
    });
  });

  it('on StoreEntriesAction, adds entries', () => {
    const entries = getEntries(2);

    const newState = notificationsReducer(
      defaultNotificationState,
      new StoreEntriesAction(entries)
    );
    expect(newState).toEqual({
      ...defaultNotificationState,
      entries: {
        [entries[0].id]: entries[0],
        [entries[1].id]: entries[1]
      }
    });
  });
});
