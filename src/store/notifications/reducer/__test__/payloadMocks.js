import faker from 'faker-js/faker';

export function getNotifications(comments, discussions) {
  const data = [];
  for (let i = 0; i < comments; i++) {
    data.push({
      id: faker.datatype.number(100000),
      attributes: {
        blog: '1',
        comment: faker.datatype.number(100000),
        entry: '1',
        state: 'new'
      }
    });
  }
  for (let i = 0; i < discussions; i++) {
    data.push({
      id: faker.datatype.number(100000),
      attributes: {
        blog: faker.datatype.number(10),
        comment: faker.datatype.number(100000),
        entry: String(i + 5),
        state: 'new'
      }
    });
  }
  return data;
}

export function getEntries(number) {
  const data = [];
  for (let i = 0; i < number; i++) {
    data.push({
      id: faker.datatype.number(100000),
      attributes: {
        content: faker.random.words,
        settings: { permissions: {} },
        state: 'published',
        tags: [faker.random.words]
      }
    });
  }
  return data;
}

export function getComments(number) {
  const data = [];
  for (let i = 0; i < number; i++) {
    data.push({
      id: faker.datatype.number(100000),
      attributes: {
        content: faker.random.words,
        'created-at': faker.date.past,
        'updated-at': faker.date.past
      }
    });
  }
  return data;
}
