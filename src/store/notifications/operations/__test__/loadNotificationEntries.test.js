import { loadNotificationEntries } from '@/store/notifications/operations';
import { getSelectedEntriesApi } from '@/api/entries';

import { getState } from './_utils';
import { StoreEntriesAction } from '@/store/notifications/actions/StoreEntriesAction';
import { vi, describe, it } from 'vitest';

vi.mock('@/api/entries', () => ({
  getSelectedEntriesApi: vi.fn()
}));

describe('loadNotificationEntries', () => {
  it('calls api, dispatches a StoreEntriesAction', async () => {
    const data = {
      1: {
        id: '1',
        attributes: {
          content: 'my content'
        }
      },
      2: {
        id: '2',
        attributes: {
          content: 'my content'
        }
      }
    };
    getSelectedEntriesApi.mockResolvedValue({ data });
    const dispatch = vi.fn();

    await loadNotificationEntries({
      entryIds: ['123', '456'],
      token: '999'
    })(dispatch, getState);

    expect(getSelectedEntriesApi).toHaveBeenCalledWith({
      ids: ['123', '456'],
      token: '999'
    });
    expect(dispatch).toHaveBeenCalledWith(new StoreEntriesAction(data));
  });
});
