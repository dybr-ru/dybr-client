import { loadNotifications } from '@/store/notifications/operations';
import { getNewNotificationsApi } from '@/api/notifications';

import { ACTIVE_PROFILE, getState } from './_utils';
import { StoreNotificationsAction } from '@/store/notifications/actions/StoreNotificationsAction';

jest.mock('api/notifications', () => ({
  getNewNotificationsApi: jest.fn()
}));

describe('loadNotifications', () => {
  it('calls api, dispatches a StoreNotificationsAction', async () => {
    const data = [
      {
        id: '3',
        attributes: {
          blog: '25982',
          comment: '10881002',
          entry: '1686688',
          state: 'new'
        }
      },
      {
        id: '4',
        attributes: {
          blog: '25982',
          comment: '10881003',
          entry: '1686688',
          state: 'new'
        }
      }
    ];
    getNewNotificationsApi.mockResolvedValue({ data });
    const dispatch = jest.fn();

    await loadNotifications({ token: '999', activeProfileId: '1' })(
      dispatch,
      getState
    );

    expect(getNewNotificationsApi).toHaveBeenCalledWith({
      profileId: ACTIVE_PROFILE,
      token: '999'
    });
    expect(dispatch).toHaveBeenCalledWith(new StoreNotificationsAction(data));
  });

  it('calls api, but does not dispatch a StoreNotificationsAction if no new notification ids', async () => {
    const data = [
      {
        id: '1',
        attributes: {
          blog: '25982',
          comment: '10881002',
          entry: '1686688',
          state: 'new'
        }
      },
      {
        id: '2',
        attributes: {
          blog: '25982',
          comment: '10881003',
          entry: '1686688',
          state: 'new'
        }
      }
    ];
    getNewNotificationsApi.mockResolvedValue({ data });
    const dispatch = jest.fn();

    await loadNotifications({ token: '999', activeProfileId: '1' })(
      dispatch,
      getState
    );

    expect(getNewNotificationsApi).toHaveBeenCalledWith({
      profileId: ACTIVE_PROFILE,
      token: '999'
    });
    expect(dispatch).toHaveBeenCalledTimes(0);
  });
});
