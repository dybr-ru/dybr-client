import { initialState } from '@/store/_setup/initialState';

export const ACTIVE_PROFILE = '1';
export const getState = () => ({
  ...initialState,
  user: {
    activeProfile: ACTIVE_PROFILE,
    token: '999',
    profiles: { 1: { hello: 'world' } }
  },

  notifications: {
    items: {
      '1': {
        id: '1',
        blog: '666',
        comment: '9000',
        entry: '123',
        state: 'new'
      },
      '2': {
        id: '2',
        blog: '666',
        comment: '9001',
        entry: '456',
        state: 'new'
      }
    },
    entries: {}
  }
});
