import { ACTIVE_PROFILE, getState } from './_utils';
import { MarkNotificationReadAction } from '@/store/notifications/actions/MarkNotificationReadAction';
import { updateNotificationAPI } from '@/api/notifications';
import { readComments } from '../readComments';
import { vi, describe, it } from 'vitest';

vi.mock('@/api/notifications', () => ({
  updateNotificationAPI: vi.fn()
}));

describe('readComments', () => {
  it('calls api, dispatches a MarkNotificationsReadAction', async () => {
    const data = {
      status: 200
    };
    updateNotificationAPI.mockResolvedValue({ data });
    const dispatch = vi.fn();

    await readComments({
      commentIds: ['9000', '9001'],
      token: '123asdf',
      activeProfileId: '1'
    })(dispatch, getState);

    expect(updateNotificationAPI).toHaveBeenCalledTimes(2);
    expect(updateNotificationAPI).toHaveBeenNthCalledWith(1, {
      profileId: ACTIVE_PROFILE,
      data: {
        id: '1',
        state: 'read'
      },
      token: '123asdf'
    });
    expect(updateNotificationAPI).toHaveBeenNthCalledWith(2, {
      profileId: ACTIVE_PROFILE,
      data: {
        id: '2',
        state: 'read'
      },
      token: '123asdf'
    });

    expect(dispatch).toHaveBeenCalledTimes(2);
    expect(dispatch).toHaveBeenNthCalledWith(
      1,
      new MarkNotificationReadAction('1')
    );
    expect(dispatch).toHaveBeenNthCalledWith(
      2,
      new MarkNotificationReadAction('2')
    );
  });
});
