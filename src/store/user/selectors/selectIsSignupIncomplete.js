import { createSelector } from 'reselect';
import { selectProfiles } from '@/store/user/selectors/selectProfiles';

export const selectIsSignupIncomplete = createSelector(
  [selectProfiles],
  profiles => Object.keys(profiles || {}).length <= 0
);
