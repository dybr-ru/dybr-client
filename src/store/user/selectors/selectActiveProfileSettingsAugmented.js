import { createSelector } from 'reselect';
import { selectActiveProfileAugmented } from '@/store/user/selectors/selectActiveProfileAugmented';

export const selectActiveProfileSettingsAugmented = createSelector(
  selectActiveProfileAugmented,
  profile => profile.settings
);
