import { createSelector } from 'reselect';
import { selectUser } from '@/store/user/selectors/selectUser';

export const selectProfiles = createSelector(selectUser, user => user.profiles);
