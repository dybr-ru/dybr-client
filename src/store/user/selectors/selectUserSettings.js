import { createSelector } from 'reselect';
import { selectUser } from '@/store/user/selectors/selectUser';

export const selectUserSettings = createSelector(
  [selectUser],
  user => user.settings || {}
);
