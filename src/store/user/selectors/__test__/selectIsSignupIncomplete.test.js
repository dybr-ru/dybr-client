import cloneDeep from 'lodash-es/cloneDeep';
import { describe, expect, it } from 'vitest';

import { initialState } from '@/store/_setup/initialState';
import { selectIsSignupIncomplete } from '@/store/user/selectors/selectIsSignupIncomplete';

describe('selectIsSignupIncomplete', () => {
  it('selects a flag showing if signup is incomplete', () => {
    let state = cloneDeep(initialState);

    expect(selectIsSignupIncomplete(state)).toEqual(true);
  });

  it('selects a flag showing if signup is incomplete', () => {
    let state = cloneDeep(initialState);
    state.user.profiles = { 1: { id: '1' } };
    expect(selectIsSignupIncomplete(state)).toEqual(false);
  });
});
