import cloneDeep from 'lodash-es/cloneDeep';
import { describe, expect, it } from 'vitest';

import defaultProfileSettings from '@/configs/defaultProfileSettings';
import { initialState } from '@/store/_setup/initialState';
import { selectInactiveProfilesAugmented } from '@/store/user/selectors/selectInactiveProfilesAugmented';

describe('selectInactiveProfilesAugmented', () => {
  it('selects inactive profiles from init state', () => {
    let state = cloneDeep(initialState);

    expect(
      selectInactiveProfilesAugmented(state, { activeProfileId: '1' })
    ).toEqual([]);
  });

  it('selects inactive profiles ', () => {
    let state = cloneDeep(initialState);
    state.user.profiles = { 1: { id: '1' }, 2: { id: '2' } };
    expect(
      selectInactiveProfilesAugmented(state, { activeProfileId: '2' })
    ).toEqual([{ id: '1', settings: defaultProfileSettings }]);
  });

  it('selects inactive profiles, throws away active profile, keeps custom properties ', () => {
    let state = cloneDeep(initialState);
    state.user.activeProfile = '1';
    state.user.profiles = {
      1: { id: '1' },
      2: { id: '2', settings: defaultProfileSettings, currentDesign: '666' }
    };
    expect(
      selectInactiveProfilesAugmented(state, { activeProfileId: '1' })
    ).toEqual([
      { id: '2', settings: defaultProfileSettings, currentDesign: '666' }
    ]);
  });
});
