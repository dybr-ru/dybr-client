import { createSelector } from 'reselect';
import { selectUser } from '@/store/user/selectors/selectUser';

export const selectUserData = createSelector(selectUser, user => user.userData);
