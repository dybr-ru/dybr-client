import { createSelector } from 'reselect';
import { selectUser } from '@/store/user/selectors/selectUser';

export const selectActiveProfile = createSelector(
  selectUser,
  (state, { activeProfileId }) => activeProfileId,
  (user, activeProfileId) => user.profiles[activeProfileId] || {}
);
