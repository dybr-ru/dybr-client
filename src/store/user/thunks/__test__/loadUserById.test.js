import cloneDeep from 'lodash-es/cloneDeep';
import { vi, describe, it } from 'vitest';

import { switchProfileApi } from '@/api/auth';
import { getUserData } from '@/api/users';
import { ClearDesignsAction } from '@/store/designer/actions/ClearDesignsAction';
import { ClearListsAction } from '@/store/lists/actions';
import { useTokenState } from '@/store/localStorage/useTokenState';
import { StoreUserDataAction } from '@/store/user/actions/StoreUserDataAction';
import { loadUserById } from '@/store/user/thunks/loadUserById';

import { getState } from './_utils';

vi.mock('@/api/users', () => ({
  getUserData: vi.fn()
}));

vi.mock('@/api/auth', () => ({
  switchProfileApi: vi.fn()
}));

vi.mock('store/localStorage/useTokenState', () => {
  const mySetToken = vi.fn();

  return {
    useTokenState: () => {
      console.log('useTokenState mock is called');
      return ['123asdf', mySetToken];
    }
  };
});

describe('loadUserById', () => {
  it('calls api, dispatches a StoreUserDataAction', async () => {
    const data = {
      id: '1',
      activeProfile: '20795',
      email: 'myemail@gmail.com',
      profiles: [{ id: 20795 }]
    };

    getUserData.mockResolvedValue({ data });
    const dispatch = vi.fn();
    const [token, setToken] = useTokenState();

    await loadUserById({
      setToken,
      token,
      userId: '987',
      activeProfileId: '234',
      navigate: () => {}
    })(dispatch, getState);

    expect(getUserData).toHaveBeenCalledWith({
      userId: '987',
      token: '123asdf'
    });
    expect(dispatch).toHaveBeenCalledWith(new StoreUserDataAction(data));
  });

  it('sets activeProfile if wrong profile in store', async () => {
    const dispatch = vi.fn();
    // no active profile encoded here
    const initToken =
      'initToken.eyJleHAiOjE2MDI1MjQ0MTQsInN1YiI6IjUyMzAifQ.asdf';

    const data = {
      id: '1',
      activeProfile: '777',
      email: 'myemail@gmail.com',
      settings: {},
      profiles: [{ id: '666' }, { id: '777' }]
    };

    getUserData.mockResolvedValue({ data });
    // profile 777 is encoded here:
    const token777 =
      'token777.eyJleHAiOjE2MDI1MjQ0MTQsInN1YiI6IjUyMzAiLCJhdWQiOiI3NzcifQ.asdf';
    const switchData = {
      accessToken: token777,
      status: 'ok'
    };
    switchProfileApi.mockResolvedValue(switchData);
    const [, setToken] = useTokenState();

    await loadUserById({
      setToken,
      token: initToken,
      userId: '987',
      activeProfileId: '0',
      navigate: () => {}
    })(dispatch, getState);

    expect(getUserData).toHaveBeenCalledWith({
      userId: '987',
      token: initToken
    });
    expect(switchProfileApi).toHaveBeenCalledWith({
      profileId: '777',
      token: initToken
    });
    expect(dispatch).toHaveBeenCalledWith(new StoreUserDataAction(data));
    expect(dispatch).toHaveBeenCalledWith(new ClearListsAction());
    expect(dispatch).toHaveBeenCalledWith(new ClearDesignsAction());

    expect(setToken).toHaveBeenCalledWith(token777);
  });

  it('sets activeProfile to the first profile if no activeProfile in store', async () => {
    const dispatch = vi.fn();
    // no active profile encoded here
    const initToken =
      'initToken.eyJleHAiOjE2MDI1MjQ0MTQsInN1YiI6IjUyMzAifQ.asdf';

    const getInitState = () => {
      const state = cloneDeep(getState());
      state.user.activeProfile = '0';
      return state;
    };

    const data = {
      id: '1',
      email: 'myemail@gmail.com',
      settings: {},
      profiles: [{ id: '666' }, { id: '777' }]
    };

    getUserData.mockResolvedValue({ data });
    // profile 666 is encoded here:
    const token666 =
      'token666.eyJleHAiOjE2MDI1MjQ0MTQsInN1YiI6IjUyMzAiLCJhdWQiOiI2NjYifQ.asdf';
    const switchData = {
      accessToken: token666,
      status: 'ok'
    };
    switchProfileApi.mockResolvedValue(switchData);
    const [, setToken] = useTokenState();
    await loadUserById({
      setToken,
      token: initToken,
      userId: '987',
      navigate: () => {}
    })(dispatch, getInitState);

    expect(getUserData).toHaveBeenCalledWith({
      userId: '987',
      token: initToken
    }); // userId, token
    expect(switchProfileApi).toHaveBeenCalledWith({
      profileId: '666',
      token: initToken
    });
    expect(dispatch).toHaveBeenCalledWith(new StoreUserDataAction(data));
    expect(dispatch).toHaveBeenCalledWith(new ClearListsAction());
    expect(dispatch).toHaveBeenCalledWith(new ClearDesignsAction());
    expect(setToken).toHaveBeenCalledWith(token666);
  });

  it('sets activeProfile to "0" if the user is new and there are no profiles yet', async () => {
    const dispatch = vi.fn();
    // no active profile encoded here
    const initToken =
      'initToken.eyJleHAiOjE2MDI1MjQ0MTQsInN1YiI6IjUyMzAifQ.asdf';

    const data = {
      id: '1',
      email: 'myemail@gmail.com',
      settings: {}
    };

    getUserData.mockResolvedValue({ data });

    switchProfileApi.mockResolvedValue({});
    const [, setToken] = useTokenState();
    await loadUserById({
      setToken,
      token: initToken,
      userId: '987',
      activeProfileId: '0',
      navigate: () => {}
    })(dispatch, getState);

    expect(getUserData).toHaveBeenCalledWith({
      userId: '987',
      token: initToken
    }); // userId, token
    expect(switchProfileApi).toHaveBeenCalledTimes(0);

    expect(dispatch).toHaveBeenCalledWith(new StoreUserDataAction(data));
  });
});
