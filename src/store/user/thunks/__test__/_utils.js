import { initialState } from '@/store/_setup/initialState';

export const ACTIVE_PROFILE = '666';
export const getState = () => ({
  ...initialState,
  user: {
    userData: {},
    activeProfile: ACTIVE_PROFILE,
    userId: '1',
    token:
      'asdfg.eyJhdWQiOiIyMDc5NSIsImV4cCI6MTU5Mzk2NzE5Niwic3ViIjoiNTIzMCJ9.qwerty',
    profiles: {
      666: { id: 666 },
      1: { id: 1 }
    }
  },
  notifications: {}
});
