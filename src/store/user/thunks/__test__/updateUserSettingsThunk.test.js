import { updateUserApi } from '@/api/users';
import { getState } from '@/store/user/thunks/__test__/_utils';
import { updateUserSetting } from '@/store/user/thunks/updateUserSettingThunk';
import { SetUserSettingsAction } from '@/store/user/actions/SetUserSettingsAction';
import { vi, describe, it } from 'vitest';

vi.mock('@/api/users', () => ({
  updateUserApi: vi.fn()
}));

describe('updateUserSetting', () => {
  it('calls api, dispatches a SetUserSettingsAction', async () => {
    const data = {
      status: 'ok'
    };

    updateUserApi.mockResolvedValue({ data });
    const dispatch = vi.fn();

    const token = '1234asdf';
    await updateUserSetting('hello', 'world', token, '1')(dispatch, getState);

    const expectedSettings = { hello: 'world' };
    expect(updateUserApi).toHaveBeenCalledWith({
      data: { settings: expectedSettings },
      userId: '1',
      token
    }); // userId
    expect(dispatch).toHaveBeenCalledWith(
      new SetUserSettingsAction(expectedSettings)
    );
  });

  it('keeps old settings intact', async () => {
    const data = {
      status: 'ok'
    };

    updateUserApi.mockResolvedValue({ data });
    const dispatch = vi.fn();

    const token = '1234asdf';
    await updateUserSetting(
      'hello',
      'world',
      token,
      '1'
    )(dispatch, () => {
      const state = getState();
      return {
        ...state,
        user: {
          ...state.user,
          settings: {
            mySetting: 'should be still there'
          }
        }
      };
    });

    const expectedSetting = {
      hello: 'world',
      mySetting: 'should be still there'
    };
    expect(updateUserApi).toHaveBeenCalledWith({
      data: { settings: expectedSetting },
      userId: '1',
      token
    }); // userId
    expect(dispatch).toHaveBeenCalledWith(
      new SetUserSettingsAction(expectedSetting)
    );
  });
});
