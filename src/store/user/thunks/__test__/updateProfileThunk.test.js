import { updateProfileApi } from '@/api/profiles';
import { getState } from '@/store/user/thunks/__test__/_utils';
import { UpdateProfileAction } from '@/store/user/actions/UpdateProfileAction';
import { updateProfileThunk } from '@/store/user/thunks/updateProfileThunk';
import { vi, describe, it } from 'vitest';

vi.mock('@/api/profiles', () => ({
  updateProfileApi: vi.fn()
}));

describe('updateProfileThunk', () => {
  it('calls api, dispatches a UpdateProfileAction', async () => {
    const data = {
      status: 'ok'
    };

    updateProfileApi.mockResolvedValue({ data });
    const dispatch = vi.fn();

    const profileData = { hello: 'world', description: 'how are you' };
    await updateProfileThunk({
      data: profileData,
      token: '1234asdf',
      activeProfileId: 124
    })(dispatch, getState);

    expect(updateProfileApi).toHaveBeenCalledWith({
      data: profileData,
      activeProfileId: 124,
      token: '1234asdf'
    });
    expect(dispatch).toHaveBeenCalledWith(
      new UpdateProfileAction(profileData, 124)
    );
  });
});
