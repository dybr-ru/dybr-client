import { updateProfileApi } from '@/api/profiles';
import { getState } from '@/store/user/thunks/__test__/_utils';
import { updateProfileSetting } from '@/store/user/thunks/updateProfileSetting';
import { UpdateProfileAction } from '@/store/user/actions/UpdateProfileAction';
import { vi, describe, it } from 'vitest';

vi.mock('@/api/profiles', () => ({
  updateProfileApi: vi.fn()
}));

describe('updateProfileSetting', () => {
  // profile 666 is encoded here:
  const token =
    'token666.eyJleHAiOjE2MDI1MjQ0MTQsInN1YiI6IjUyMzAiLCJhdWQiOiI2NjYifQ.asdf';

  it('calls api, dispatches a UpdateProfileAction', async () => {
    const data = {
      status: 'ok'
    };

    updateProfileApi.mockResolvedValue({ data });
    const dispatch = vi.fn();

    await updateProfileSetting('hello', 'world', token)(dispatch, getState);

    const expectedData = { settings: { hello: 'world' } };
    expect(updateProfileApi).toHaveBeenCalledWith({
      data: expectedData,
      activeProfileId: 666,
      token
    });
    expect(dispatch).toHaveBeenCalledWith(
      new UpdateProfileAction(expectedData, 666)
    );
  });

  it('update nested settings', async () => {
    const data = {
      status: 'ok'
    };

    updateProfileApi.mockResolvedValue({ data });
    const dispatch = vi.fn();

    await updateProfileSetting(
      'hello.my.wonderful',
      'world',
      token
    )(dispatch, getState);

    const expectedData = {
      settings: { hello: { my: { wonderful: 'world' } } }
    };
    expect(updateProfileApi).toHaveBeenCalledWith({
      data: expectedData,
      activeProfileId: 666,
      token
    });
    expect(dispatch).toHaveBeenCalledWith(
      new UpdateProfileAction(expectedData, 666)
    );
  });
});
