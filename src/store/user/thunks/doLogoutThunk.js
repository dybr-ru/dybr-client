import { LogoutAction } from '@/store/user/actions/LogoutAction';

export const doLogoutThunk = (setToken) => (dispatch) => {
  setToken('');
  return dispatch(new LogoutAction());
};
