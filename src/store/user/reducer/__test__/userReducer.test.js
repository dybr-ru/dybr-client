import faker from 'faker-js/faker';
import { describe, expect, it } from 'vitest';

import { defaultUserState } from '@/store/_setup/initialState';
import { LogoutAction } from '@/store/user/actions/LogoutAction';
import { SetActiveProfileIdAction } from '@/store/user/actions/SetActiveProfileIdAction';
import { SetProfileAction } from '@/store/user/actions/SetProfileAction';
import { SetUserSettingsAction } from '@/store/user/actions/SetUserSettingsAction';
import { StoreUserDataAction } from '@/store/user/actions/StoreUserDataAction';
import { UpdateProfileAction } from '@/store/user/actions/UpdateProfileAction';
import { userReducer } from '@/store/user/reducer/userReducer';

function getFakeUserData() {
  const activeProfile = faker.datatype.number(100000);
  return {
    activeProfile: activeProfile,
    createdAt: faker.date.past,
    email: faker.date.past,
    id: faker.datatype.number(100000),
    isAdult: false,
    meta: { profiles: 0 },
    profiles: [
      {
        blogSlug: faker.random.word,
        blogTitle: faker.random.word,
        createdAt: faker.date.past,
        description: faker.random.words,
        id: activeProfile,
        isCommunity: false,
        meta: { entries: { public: 0 } },
        nickname: faker.random.word,
        settings: {},
        type: 'profiles',
        updatedAt: faker.date.past
      },
      {
        blogSlug: faker.random.word,
        blogTitle: faker.random.word,
        createdAt: faker.date.past,
        description: faker.random.words,
        id: faker.datatype.number(100000),
        isCommunity: false,
        meta: { entries: { public: 0 } },
        nickname: faker.random.word,
        settings: {},
        type: 'profiles',
        updatedAt: faker.date.past
      }
    ],
    relationshipNames: ['profiles'],
    termsOfService: true,
    type: 'users',
    updatedAt: faker.date.past
  };
}

describe('user reducer', () => {
  it('returns given state on unknown action', () => {
    const state = { whatever: [{ listId: 1, data: 'some data' }] };
    const newState = userReducer(state, {
      type: 'some_action',
      payload: { some: 'data' }
    });
    expect(newState).toEqual(state);
  });

  it('on StoreUserDataAction, correctly stores the data', () => {
    const fakeUserData = getFakeUserData();
    const newState = userReducer(
      defaultUserState,
      new StoreUserDataAction(fakeUserData)
    );
    expect(newState).toEqual({
      ...defaultUserState,
      userData: {
        createdAt: fakeUserData.createdAt,
        updatedAt: fakeUserData.updatedAt,
        email: fakeUserData.email,
        isAdult: fakeUserData.isAdult,
        termsOfService: fakeUserData.termsOfService
      },
      activeProfile: defaultUserState.activeProfile,
      profiles: {
        [fakeUserData.profiles[0].id]: fakeUserData.profiles[0],
        [fakeUserData.profiles[1].id]: fakeUserData.profiles[1]
      }
    });
  });

  it('on SetActiveProfileAction, saves active profile id', () => {
    const activeProfile = '9876';
    const newState = userReducer(
      defaultUserState,
      new SetActiveProfileIdAction(activeProfile)
    );
    expect(newState).toEqual({
      ...defaultUserState,
      activeProfile
    });
  });

  it('on SetProfileAction, saves profile', () => {
    const fakeId = faker.datatype.number(100000);
    const fakeProfile = {
      blogSlug: faker.random.word,
      blogTitle: faker.random.word,
      createdAt: faker.date.past,
      description: faker.random.words,
      id: fakeId,
      isCommunity: false,
      meta: { entries: { public: 0 } },
      nickname: faker.random.word,
      settings: {},
      type: 'profiles',
      updatedAt: faker.date.past
    };
    const newState = userReducer(
      defaultUserState,
      new SetProfileAction(fakeProfile)
    );
    expect(newState).toEqual({
      ...defaultUserState,
      profiles: {
        ...defaultUserState.profiles,
        [fakeId]: fakeProfile
      }
    });
  });

  it('on UpdateProfileAction, updates profile', () => {
    const fakeProfileData = {
      description: 'updated description'
    };
    const activeProfile = defaultUserState.activeProfile;
    const newState = userReducer(
      defaultUserState,
      new UpdateProfileAction(fakeProfileData, activeProfile)
    );
    expect(newState).toEqual({
      ...defaultUserState,
      profiles: {
        ...defaultUserState.profiles,
        [activeProfile]: {
          ...defaultUserState.profiles[activeProfile],
          description: 'updated description'
        }
      }
    });
  });

  it('on SetUserSettingsAction, saves settings', () => {
    const settings = { hello: 'world' };
    const newState = userReducer(
      defaultUserState,
      new SetUserSettingsAction(settings)
    );
    expect(newState).toEqual({
      ...defaultUserState,
      settings
    });
  });

  it('on LogoutAction, restores initial state of the store', () => {
    const newState = userReducer(
      {
        ...defaultUserState,
        activeProfile: '666',
        userData: getFakeUserData()
      },
      new LogoutAction()
    );
    expect(newState).toEqual(defaultUserState);
  });
});
