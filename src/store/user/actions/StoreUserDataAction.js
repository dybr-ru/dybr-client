export class StoreUserDataAction {
  static type = 'STORE_USER_DATA';

  constructor(data) {
    this.data = data;
    this.type = StoreUserDataAction.type;
  }
}
