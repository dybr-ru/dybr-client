export class LogoutAction {
  static type = 'LOGOUT_USER';

  constructor() {
    this.type = LogoutAction.type;
  }
}
