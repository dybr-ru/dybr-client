export class SetUserSettingsAction {
  static type = 'User/SET_SETTINGS';
  constructor(settings) {
    this.settings = settings;
    this.type = SetUserSettingsAction.type;
  }
}
