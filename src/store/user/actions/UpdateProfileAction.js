export class UpdateProfileAction {
  static type = 'User/UPDATE_PROFILE';
  constructor(data, id) {
    this.data = data;
    this.id = id;
    this.type = UpdateProfileAction.type;
  }
}
