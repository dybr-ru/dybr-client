export class SetProfileAction {
  static type = 'User/SET_PROFILE';
  constructor(profile) {
    this.profile = profile;
    this.type = SetProfileAction.type;
  }
}
