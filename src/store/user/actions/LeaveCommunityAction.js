export class LeaveCommunityAction {
  static type = 'User/LEAVE_COMMUNITY';
  constructor(profileId, communityId) {
    this.profileId = profileId;
    this.communityId = communityId;
    this.type = LeaveCommunityAction.type;
  }
}
