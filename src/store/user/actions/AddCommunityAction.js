export class AddCommunityAction {
  static type = 'User/ADD_COMMUNITY';
  constructor(profileId, community) {
    this.profileId = profileId;
    this.community = community;
    this.type = AddCommunityAction.type;
  }
}
