export class SetCommunitiesAction {
  static type = 'User/SET_COMMUNITIES';
  constructor(profileId, communities) {
    this.profileId = profileId;
    this.communities = communities;
    this.type = SetCommunitiesAction.type;
  }
}
