export class SetActiveProfileIdAction {
  static type = 'user/SET_CURRENT_PROFILE';

  constructor(activeProfile) {
    this.activeProfile = activeProfile;
    this.type = SetActiveProfileIdAction.type;
  }
}
