import { runMigrationToLS } from '@/store/localStorage/migrationToLS';
import { describe, expect, test, vi } from 'vitest';

describe('Migrate from persisted Redux state to local storage ', () => {
  test('does not fail if no persisted state', () => {
    const state = undefined;

    const setToken = vi.fn();

    const {
      commentLocalStorageState,
      entryLocalStorageState
    } = runMigrationToLS(state, setToken);

    expect(setToken).toBeCalledTimes(0);
    expect(commentLocalStorageState).toEqual({});
    expect(entryLocalStorageState).toEqual({});
  });

  test('does not fail if no persisted state', () => {
    const token666 =
      'token666.eyJleHAiOjE2MDI1MjQ0MTQsInN1YiI6IjUyMzAiLCJhdWQiOiI2NjYifQ.asdf';
    const userState = JSON.stringify({ token: token666 });

    const formsState = JSON.stringify({
      byProfile: {
        131: {
          entryDrafts: {
            '131.0': {
              id: '0',
              content: 'content of entry draft 0',
              tags: ['a', 'b', 'c']
            }
          },
          commentDrafts: {
            19547: {
              eid: '19547',
              content: 'content of comment draft 19547'
            }
          }
        },
        25982: {
          entryDrafts: {
            '132.0': {
              id: '0',
              content: 'content of community 132 entry draft 0',
              tags: ['a', 'b', 'c']
            },
            29485756: {
              id: '29485756',
              content: 'content of entry draft 29485756',
              tags: ['d', 'e', 'f']
            }
          },
          commentDrafts: {
            19548: {
              eid: '19548',
              content: 'content of comment draft 19548'
            },
            19549: {
              eid: '19549',
              content: ''
            }
          }
        }
      }
    });

    const state = JSON.stringify({
      user: userState,
      forms: formsState
    });

    const setToken = vi.fn();

    const {
      commentLocalStorageState,
      entryLocalStorageState
    } = runMigrationToLS(state, setToken);

    expect(setToken).toBeCalledWith(token666);

    expect(entryLocalStorageState).toEqual({
      131: {
        '131.0': {
          id: '0',
          content: 'content of entry draft 0',
          tags: ['a', 'b', 'c']
        }
      },
      25982: {
        '132.0': {
          id: '0',
          content: 'content of community 132 entry draft 0',
          tags: ['a', 'b', 'c']
        },
        29485756: {
          id: '29485756',
          content: 'content of entry draft 29485756',
          tags: ['d', 'e', 'f']
        }
      }
    });
    expect(commentLocalStorageState).toEqual({
      131: {
        19547: {
          eid: '19547',
          content: 'content of comment draft 19547'
        }
      },
      25982: {
        19548: {
          eid: '19548',
          content: 'content of comment draft 19548'
        }
      }
    });
  });
});
