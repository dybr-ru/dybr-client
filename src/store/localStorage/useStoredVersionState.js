import createPersistedState from 'use-persisted-state-18';

export const useStoredVersionState = createPersistedState('storedVersion');
