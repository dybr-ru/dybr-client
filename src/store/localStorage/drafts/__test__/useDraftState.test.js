import { describe, expect, test, vi, beforeAll } from "vitest";
import { useEntryDraftById } from '@/store/localStorage/drafts/useEntryDraftById';
import { useCommentDraftById } from '@/store/localStorage/drafts/useCommentDraftById';
import { renderHook } from '@testing-library/react';

let oldState = {
  20007: {
    entryDrafts: {
      123: { id: '123', content: 'entryDraft123' },
      124: { id: '124', content: 'entryDraft124' },
      '999.0': { id: '999.0', content: 'entryDraft999.0' }
    },
    commentDrafts: {
      123: { eid: '123', content: 'comment123' },
      124: { eid: '124', content: 'comment124' },
      125: { eid: '125', content: 'comment125' }
    }
  }
};

let localStorage = {
  entryDrafts: {
    // profileId
    666: {
      123: { id: '123', content: 'entryDraft123' },
      124: { id: '124', content: 'entryDraft124' },
      '999.0': { id: '999.0', content: 'entryDraft999.0' }
    }
  },
  commentDrafts: {
    // profileId
    666: {
      123: { eid: '123', content: 'comment123' },
      124: { eid: '124', content: 'comment124' },
      125: { eid: '125', content: 'comment125' }
    },
    777: {
      234: { eid: '234', content: 'comment234' }
    }
  }
};

vi.mock('@/store/localStorage/drafts/useDraftState', () => {
  return {
    useEntryDraftState: () => {
      console.log('useEntryDraftState mock is called');
      return [localStorage.entryDrafts, vi.fn()];
    },
    useCommentDraftState: () => [localStorage.commentDrafts, vi.fn()]
  };
});


describe('useDraftState', () => {
  test('useEntryDraftByEid returns the entry draft', () => {
    const { result } = renderHook(() => useEntryDraftById({
      activeProfileId: '666',
      entryId: '124',
      blog: { isCommunity: false }
    }));
    expect(result.current).toEqual({ id: '124', content: 'entryDraft124' });
  });

  test('useEntryDraftByEid selector returns the new community entry draft', () => {
    const { result } = renderHook(() => useEntryDraftById({
      activeProfileId: '666',
      entryId: '0',
      blog: {
        isCommunity: true,
        id: '999'
      }
    }));
    expect(result.current).toEqual({ id: '999.0', content: 'entryDraft999.0' });
  });

  test('useCommentDraftByEid selector returns the comment draft', () => {
    const { result } = renderHook(() => useCommentDraftById({
      activeProfileId: '666',
      commentId: '125'
    }));
    expect(result.current).toEqual({ eid: '125', content: 'comment125' });
  });
});
