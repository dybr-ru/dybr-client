import { describe, expect, test, vi } from "vitest";
import { setEntryDraft } from '@/store/localStorage/drafts/useSetEntryDraft';

let localStorage = {
  entryDrafts: {
    // profileId
    666: {
      123: { id: '123', content: 'entryDraft123' },
      124: { id: '124', content: 'entryDraft124' },
      '999.0': { id: '999.0', content: 'entryDraft999.0' }
    }
  }
};

describe('setEntryDraft', () => {
  test('setEntryDraft return new entry drafts map', () => {
    const draftSetter = setEntryDraft(
      {
        id: '124',
        content: 'asdf',
        title: 'qwert',
        tags: ['1asdf', '2asdf']
      },
      { isCommunity: false },
      '666'
    );

    expect(draftSetter(localStorage.entryDrafts)).toEqual({
      666: {
        123: { id: '123', content: 'entryDraft123' },
        124: {
          id: '124',
          content: 'asdf',
          title: 'qwert',
          tags: ['1asdf', '2asdf']
        },
        '999.0': { id: '999.0', content: 'entryDraft999.0' }
      }
    });
  });

  test('setEntryDraft creates entry drafts map', () => {
    const draftSetter = setEntryDraft(
      {
        id: '124',
        content: 'asdf',
        title: 'qwert',
        tags: ['1asdf', '2asdf']
      },
      { isCommunity: false },
      '666'
    );

    expect(draftSetter(undefined)).toEqual({
      666: {
        124: {
          id: '124',
          content: 'asdf',
          title: 'qwert',
          tags: ['1asdf', '2asdf']
        }
      }
    });
  });

  test('setEntryDraft removes empty draft from the map', () => {
    const draftSetter = setEntryDraft(
      {
        id: '124',
        content: '',
        title: '',
        tags: []
      },
      { isCommunity: false },
      '666'
    );

    expect(draftSetter(localStorage.entryDrafts)).toEqual({
      666: {
        123: { id: '123', content: 'entryDraft123' },
        '999.0': { id: '999.0', content: 'entryDraft999.0' }
      }
    });
  });
  test('setEntryDraft deletes draft if no content received', () => {
    const draftSetter = setEntryDraft(
      {
        id: '124'
      },
      { isCommunity: false },
      '666'
    );

    expect(draftSetter(localStorage.entryDrafts)).toEqual({
      666: {
        123: { id: '123', content: 'entryDraft123' },
        '999.0': { id: '999.0', content: 'entryDraft999.0' }
      }
    });
  });
  test('setEntryDraft does not create new entry for new activeProfileId if nothing to save', () => {
    const draftSetter = setEntryDraft(
      {
        id: '124',
        content: '',
        title: '',
        tags: []
      },
      { isCommunity: false },
      '333'
    );

    expect(draftSetter(localStorage.entryDrafts)).toEqual(
      localStorage.entryDrafts
    );
  });

  test('setEntryDraft creates new entry for new activeProfileId', () => {
    const draftSetter = setEntryDraft(
      {
        id: '124',
        content: 'asdf',
        title: 'qwert',
        tags: ['1asdf', '2asdf']
      },
      { isCommunity: false },
      '333'
    );

    expect(draftSetter(localStorage.entryDrafts)).toEqual({
      ...localStorage.entryDrafts,
      333: {
        124: {
          id: '124',
          content: 'asdf',
          title: 'qwert',
          tags: ['1asdf', '2asdf']
        }
      }
    });
  });
});
