import { describe, expect, test } from "vitest";
let state;

describe.skip('Migrate drafts from by-user to by-profile separation', () => {
  test('does not fail if no by-user drafts', () => {
    state = {
      user: { userId: '007' },
      forms: {}
    };
    const newState = formsReducer(state, {
      type: MigrateFormsAction.type,
      payload: { userId: '007', activeProfile: '666' }
    });
    expect(newState).toEqual(state);
  });

  test('migrates drafts correctly', () => {
    state = {
      user: { userId: '007' },
      forms: {
        666: {
          entryDrafts: {
            323: { id: '323', content: '666data' },
            324: { id: '324', content: '667data' }
          },
          commentDrafts: {
            423: { eid: '423', content: '668data' }
          }
        },
        '007': {
          entryDrafts: {
            123: { id: '123', content: '007data' },
            124: { id: '124', content: '008data' }
          },
          commentDrafts: {
            223: { eid: '223', content: '009data' }
          }
        }
      }
    };
    const newState = formsReducer(state, {
      type: MigrateFormsAction.type,
      userId: '007',
      activeProfileId: '23456'
    });
    expect(newState).toEqual({
      ...state,
      forms: {
        666: {
          entryDrafts: {
            323: { id: '323', content: '666data' },
            324: { id: '324', content: '667data' }
          },
          commentDrafts: {
            423: { eid: '423', content: '668data' }
          }
        },
        byProfile: {
          23456: {
            entryDrafts: {
              123: { id: '123', content: '007data' },
              124: { id: '124', content: '008data' }
            },
            commentDrafts: {
              223: { eid: '223', content: '009data' }
            }
          }
        }
      }
    });
  });
});
