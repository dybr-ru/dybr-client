import { describe, expect, test, vi } from "vitest";
import { setCommentDraft } from '@/store/localStorage/drafts/useSetCommentDraft';

let localStorage = {
  commentDrafts: {
    // profileId
    666: {
      123: { eid: '123', content: 'comment123' },
      124: { eid: '124', content: 'comment124' },
      125: { eid: '125', content: 'comment125' }
    },
    777: {
      234: { eid: '234', content: 'comment234' }
    }
  }
};

describe('setCommentDraft', () => {
  test('setCommentDraft return new drafts map', () => {
    const draftSetter = setCommentDraft(
      {
        eid: '124',
        content: 'asdf'
      },
      '666'
    );

    expect(draftSetter(localStorage.commentDrafts)).toEqual({
      666: {
        123: { eid: '123', content: 'comment123' },
        124: { eid: '124', content: 'asdf' },
        125: { eid: '125', content: 'comment125' }
      },
      777: {
        234: { eid: '234', content: 'comment234' }
      }
    });
  });

  test('setCommentDraft creates entry drafts map', () => {
    const draftSetter = setCommentDraft(
      {
        eid: '124',
        content: 'asdf'
      },
      '666'
    );

    expect(draftSetter(undefined)).toEqual({
      666: {
        124: { eid: '124', content: 'asdf' }
      }
    });
  });

  test('setCommentDraft removes empty draft from the map', () => {
    const draftSetter = setCommentDraft(
      {
        eid: '124',
        content: ''
      },
      '666'
    );

    expect(draftSetter(localStorage.commentDrafts)).toEqual({
      666: {
        123: { eid: '123', content: 'comment123' },
        125: { eid: '125', content: 'comment125' }
      },
      777: {
        234: { eid: '234', content: 'comment234' }
      }
    });
  });
  test('setCommentDraft deletes entry if no content provided', () => {
    const draftSetter = setCommentDraft(
      {
        eid: '124'
      },
      '666'
    );

    expect(draftSetter(localStorage.commentDrafts)).toEqual({
      666: {
        123: { eid: '123', content: 'comment123' },
        125: { eid: '125', content: 'comment125' }
      },
      777: {
        234: { eid: '234', content: 'comment234' }
      }
    });
  });
  test('setCommentDraft does not create new entry for new activeProfileId if nothing to save', () => {
    const draftSetter = setCommentDraft(
      {
        eid: '124',
        content: ''
      },
      '333'
    );

    expect(draftSetter(localStorage.commentDrafts)).toEqual(
      localStorage.commentDrafts
    );
  });

  test('setCommentDraft creates new entry for new activeProfileId', () => {
    const draftSetter = setCommentDraft(
      {
        eid: '124',
        content: 'asdf'
      },
      '333'
    );

    expect(draftSetter(localStorage.commentDrafts)).toEqual({
      ...localStorage.commentDrafts,
      333: {
        124: {
          eid: '124',
          content: 'asdf'
        }
      }
    });
  });
});
