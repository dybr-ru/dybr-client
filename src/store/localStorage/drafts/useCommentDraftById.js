import { useCommentDraftState } from '@/store/localStorage/drafts/useDraftState';

export const useCommentDraftById = ({ activeProfileId, commentId }) => {
  const [draft] = useCommentDraftState();

  return draft?.[activeProfileId]?.[commentId] ?? {};
};
