import createPersistedState from 'use-persisted-state-18';

export const useEntryDraftState = createPersistedState('entryDrafts');
export const useCommentDraftState = createPersistedState('commentDrafts');
