import { createSelector } from 'reselect';

export const selectAllDesigns = createSelector(
  state => state.designer.designs,
  designs => Object.values(designs)
);
