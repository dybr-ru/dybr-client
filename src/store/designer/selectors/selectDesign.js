export function selectDesign(state, designID) {
  return state.designer.designs[designID];
}
