import { it, expect } from 'vitest';
import { selectDesign } from '../index';

it('selects draft design object', () => {
  let state = { designer: { designs: {} } };
  const design1 = { id: '1', name: 'one' };
  const design2 = { id: '2', name: 'two' };
  state.designer.designs = { '1': design1, '2': design2 };

  expect(selectDesign(state, '2')).toEqual(design2);
});
