import { it, expect } from 'vitest';
import { selectDraftDesign } from '../index';

import { initialState } from '@/store/_setup/initialState';

it('selects draft design object', () => {
  let state = initialState;
  state.designer.draftDesign = 'bla';

  expect(selectDraftDesign(state)).toEqual('bla');
});
