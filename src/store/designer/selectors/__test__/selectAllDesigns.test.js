import { it, expect } from 'vitest';
import { selectAllDesigns } from '../index';

import { initialState } from '@/store/_setup/initialState';

it('selects draft design object', () => {
  let state = initialState;
  const design1 = { id: '1', name: 'one' };
  const design2 = { id: '2', name: 'two' };
  state.designer.designs = { '1': design1, '2': design2 };

  expect(selectAllDesigns(state)).toEqual([design1, design2]);
});
