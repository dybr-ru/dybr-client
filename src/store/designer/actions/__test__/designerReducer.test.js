import { designerReducer } from '@/store/designer/designerReducer';
import {
  AddDesignAction,
  CollapseDesignerAction,
  DeleteDesignAction,
  EndEditingAction,
  ExpandDesignerAction,
  StartEditingAction,
  StoreDesignsAction,
  UpdateDesignAction
} from '..';
import { defaultDesignerState } from '@/store/_setup/initialState';
import { describe, expect, it } from 'vitest';

/**
 *  structure:
 *
 *
    designerOn: false,
    designerCollapsed: false,
    designs: {
        '1': { ... }
    },

    draftDesign: {
        id: '0',
        name: "новый дизайн",
        colors: {},
        background: {},
        layout: {},
        header: {},
        fonts: {}
        ...same as design settings...
    }
 *
 */

describe('designer reducer', () => {
  it('returns given state on unknown action', () => {
    const state = { whatever: [{ listID: 1, data: 'some data' }] };
    const newState = designerReducer(state, {
      type: 'some_action',
      payload: { some: 'data' }
    });
    expect(newState).toEqual(state);
  });

  it('on AddDesignAction, adds design to the list', () => {
    const state = designerReducer(
      defaultDesignerState,
      new AddDesignAction({ id: '1', some: 'data' })
    );
    expect(state.designs).toEqual({ '1': { id: '1', some: 'data' } });
  });

  it('on CollapseDesignerAction, sets the design panel collapsed', () => {
    const state = designerReducer(defaultDesignerState, new CollapseDesignerAction());
    expect(state.designerCollapsed).toEqual(true);
  });

  it('on DeleteDesignAction, removes the design from the list', () => {
    const state = designerReducer(
      defaultDesignerState,
      new AddDesignAction({ id: '1', some: 'data' })
    );
    const newState = designerReducer(state, new DeleteDesignAction('1'));
    expect(newState.designs['1']).toBeUndefined();
  });

  it('on EndEditingAction, sets designer panel closed and removes the design draft', () => {
    const state = {
      designer: {
        designerOn: false,

        draftDesign: {
          id: '0',
          name: 'новый дизайн'
        }
      }
    };
    const newState = designerReducer(state, new EndEditingAction());
    expect(newState.designerOn).toBeFalsy();
    expect(newState.draftDesign).toEqual({});
  });

  it('on ExpandDesignerAction, sets the design panel expanded', () => {
    const state = designerReducer(defaultDesignerState, new ExpandDesignerAction());
    expect(state.designerCollapsed).toEqual(false);
  });
  it('on StartEditingAction, sets the designer active and sets the draft to whatever is provided', () => {
    const state = designerReducer(
      defaultDesignerState,
      new StartEditingAction({ id: '123', name: 'new design' })
    );
    expect(state.designerCollapsed).toEqual(false);
    expect(state.designerOn).toEqual(true);
    expect(state.draftDesign).toEqual({
      id: '123',
      name: 'new design'
    });
  });
  it('on StoreDesignsAction, stores the provided designs', () => {
    const design1 = { id: '1', name: 'one' };
    const design2 = { id: '2', name: 'two' };
    const state = designerReducer(
      defaultDesignerState,
      new StoreDesignsAction([design1, design2])
    );
    expect(state.designs['1']).toEqual(design1);
    expect(state.designs['2']).toEqual(design2);
  });
  it('on UpdateDesignAction, updates the draft design', () => {
    const state = { draftDesign: { id: '1', name: 'one' } };
    const newState = designerReducer(
      state,
      new UpdateDesignAction({ id: '1', some: 'other property' })
    );
    expect(newState.draftDesign).toEqual({
      id: '1',
      name: 'one',
      some: 'other property'
    });
  });
});
