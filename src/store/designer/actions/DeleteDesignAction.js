export class DeleteDesignAction {
  static type = 'designer/DELETE_DESIGN';
  constructor(designID) {
    this.designID = designID;
    this.type = DeleteDesignAction.type;
  }
}
