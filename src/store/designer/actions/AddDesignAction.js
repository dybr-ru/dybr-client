export class AddDesignAction {
  static type = 'designer/ADD_DESIGN';
  constructor(design) {
    this.design = design;
    this.type = AddDesignAction.type;
  }
}
