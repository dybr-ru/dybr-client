export class StartEditingAction {
  static type = 'designer/START_EDITING';
  constructor(designData) {
    this.designData = designData;
    this.type = StartEditingAction.type;
  }
}
