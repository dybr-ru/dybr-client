export class StoreDesignsAction {
  static type = 'designer/STORE_DESIGNS';
  constructor(designs) {
    this.designs = designs;
    this.type = StoreDesignsAction.type;
  }
}
