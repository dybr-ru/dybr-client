export class EndEditingAction {
  static type = 'designer/END_EDITING';
  constructor() {
    this.type = EndEditingAction.type;
  }
}
