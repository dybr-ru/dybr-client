export class ClearDesignsAction {
  static type = 'designer/CLEAR_DESIGNS';
  constructor() {
    this.type = ClearDesignsAction.type;
  }
}
