export class UpdateDesignAction {
  static type = 'designer/UPDATE';
  constructor(designData) {
    this.designData = designData;

    this.type = UpdateDesignAction.type;
  }
}
