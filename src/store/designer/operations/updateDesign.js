import { UpdateDesignAction } from '@/store/designer/actions';

export const updateDesign = data => dispatch => {
  dispatch(new UpdateDesignAction(data));
};
