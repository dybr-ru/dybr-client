import { StartEditingAction } from '@/store/designer/actions';

import defaultBlogDesign from '@/configs/defaultBlogDesign';

export const startNewDesign = name => dispatch => {
  dispatch(new StartEditingAction({ ...defaultBlogDesign, name }));
};
