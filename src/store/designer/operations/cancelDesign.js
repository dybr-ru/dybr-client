import { EndEditingAction } from '@/store/designer/actions';

export const cancelDesign = () => dispatch => {
  dispatch(new EndEditingAction());
};
