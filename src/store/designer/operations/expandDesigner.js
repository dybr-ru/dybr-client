import { ExpandDesignerAction } from '@/store/designer/actions';

export const expandDesigner = () => dispatch => {
  dispatch(new ExpandDesignerAction());
};
