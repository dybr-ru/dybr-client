import { StartEditingAction } from '@/store/designer/actions';
import { startEditingDesign } from '@/store/designer/operations';
import { vi, describe, it } from 'vitest';

const store = {
  designer: {
    designs: {
      '1': { id: '1', name: 'one' },
      '2': { id: '2', name: 'two' }
    }
  }
};

describe('startEditingDesign', () => {
  it('dispatches StartEditingAction with a copy of existing design', () => {
    const dispatch = vi.fn();

    startEditingDesign('1')(dispatch, () => store);
    expect(dispatch).toHaveBeenCalledWith(
      new StartEditingAction({ id: '1', name: 'one' })
    );
  });
});
