import { StartEditingAction } from '@/store/designer/actions';
import { startDesignerWithACopy } from '@/store/designer/operations';
import { vi, describe, it } from 'vitest';

const store = {
  designer: {
    designs: {
      '1': { id: '1', name: 'one' },
      '2': { id: '2', name: 'two' }
    }
  }
};

describe('startDesignerWithACopy', () => {
  it('dispatches StartEditingAction with a copy of existing design, id = 0 and modified name', () => {
    const dispatch = vi.fn();

    startDesignerWithACopy('1')(dispatch, () => store);
    expect(dispatch).toHaveBeenCalledWith(
      new StartEditingAction({ id: '0', name: 'one (копия)' })
    );
  });
});
