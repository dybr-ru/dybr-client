import { EndEditingAction } from '@/store/designer/actions';
import { cancelDesign } from '@/store/designer/operations';
import { vi, describe, it } from 'vitest';

it('cancelDesign dispatches EndEditingAction', () => {
  const dispatch = vi.fn();
  cancelDesign()(dispatch);
  expect(dispatch).toHaveBeenCalledWith(new EndEditingAction());
});
