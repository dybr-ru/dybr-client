import { UpdateDesignAction } from '@/store/designer/actions';
import { updateDesign } from '@/store/designer/operations';
import { vi, describe, it } from 'vitest';

describe('updateDesign', () => {
  it('dispatches a UpdateDesignAction with changed design data', async () => {
    const dispatch = vi.fn();
    const data = { colors: { text: 'red' } };
    updateDesign(data)(dispatch);
    expect(dispatch).toHaveBeenCalledWith(new UpdateDesignAction(data));
  });
});
