import { StartEditingAction } from '@/store/designer/actions';
import { startNewDesign } from '@/store/designer/operations';
import defaultBlogDesign from '@/configs/defaultBlogDesign';
import { vi, describe, it } from 'vitest';

describe('startEditingDesign', () => {
  it('dispatches StartEditingAction with a copy of default design, id = 0, and provided name', () => {
    const dispatch = vi.fn();

    startNewDesign('amazing design')(dispatch);
    expect(dispatch).toHaveBeenCalledWith(
      new StartEditingAction({
        ...defaultBlogDesign,
        id: '0',
        name: 'amazing design'
      })
    );
  });
});
