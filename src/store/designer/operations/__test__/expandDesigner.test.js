import { ExpandDesignerAction } from '@/store/designer/actions';
import { expandDesigner } from '@/store/designer/operations';
import { vi, describe, it } from 'vitest';

it('expandDesigner dispatches ExpandDesignerAction', () => {
  const dispatch = vi.fn();
  expandDesigner()(dispatch);
  expect(dispatch).toHaveBeenCalledWith(new ExpandDesignerAction());
});
