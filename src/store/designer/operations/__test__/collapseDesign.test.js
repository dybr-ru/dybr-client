import { CollapseDesignerAction } from '@/store/designer/actions';
import { collapseDesigner } from '@/store/designer/operations';
import { vi, describe, it } from 'vitest';

it('collapseDesigner dispatches CollapseDesignerAction', () => {
  const dispatch = vi.fn();
  collapseDesigner()(dispatch);
  expect(dispatch).toHaveBeenCalledWith(new CollapseDesignerAction());
});
