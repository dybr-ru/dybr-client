import { AddDesignAction, UpdateDesignAction } from '@/store/designer/actions';
import { saveDesign } from '@/store/designer/operations';
import { createDesignAPI, updateDesignAPI } from '@/api/designs';
import { vi, describe, it } from 'vitest';

import { ACTIVE_PROFILE } from './_utils';

vi.mock('@/api/designs', () => ({
  updateDesignAPI: vi.fn(),
  createDesignAPI: vi.fn()
}));

describe('saveDesign', () => {
  const token = '1234asdf';
  const activeProfileId = '666';

  it('if design has id, sends update request. If succeeded, dispatches action. Returns design id', async () => {
    updateDesignAPI.mockResolvedValue({ success: true, data: { id: '1' } });
    const dispatch = vi.fn();
    const design = { id: '123', settings: 'fffff' };
    const state = {
      user: { userData: {}, activeProfile: ACTIVE_PROFILE, token: '666' },
      designer: { draftDesign: design }
    };

    const res = await saveDesign({ token, activeProfileId })(
      dispatch,
      () => state
    );

    expect(createDesignAPI).toHaveBeenCalledTimes(0);
    expect(updateDesignAPI).toHaveBeenCalledWith({
      design,
      designId: '123',
      activeProfileId,
      token
    });
    expect(dispatch).toHaveBeenCalledWith(new UpdateDesignAction(design));
    expect(res).toBe('1');
  });

  it('if design has no id, sends create request. If succeeded, dispatches action with the data returned. Returns design id', async () => {
    const dispatch = vi.fn();
    const design = { id: '0', settings: 'fffff' };
    const state = {
      user: { userData: {}, activeProfile: activeProfileId, token },
      designer: { draftDesign: design }
    };
    const createdDesign = { id: '123', settings: 'fffff' };
    createDesignAPI.mockResolvedValue({ data: createdDesign });

    const res = await saveDesign({ token, activeProfileId })(
      dispatch,
      () => state
    );

    expect(updateDesignAPI).toHaveBeenCalledTimes(0);
    expect(createDesignAPI).toHaveBeenCalledWith({
      design,
      activeProfileId,
      token
    });
    expect(dispatch).toHaveBeenCalledWith(new AddDesignAction(createdDesign));
    expect(res).toBe('123');
  });
});
