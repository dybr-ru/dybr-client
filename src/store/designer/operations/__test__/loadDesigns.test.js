import { StoreDesignsAction } from '@/store/designer/actions';
import { loadDesigns } from '@/store/designer/operations';
import { getProfileDesignsAPI } from '@/api/designs';
import { vi, describe, it } from 'vitest';

import { ACTIVE_PROFILE, getState } from './_utils';

vi.mock('@/api/designs', () => ({
  getProfileDesignsAPI: vi.fn()
}));

describe('loadDesigns', () => {
  it('calls api, dispatches a StoreDesignsAction', async () => {
    const data = [{ id: '1' }, { id: '2' }];
    getProfileDesignsAPI.mockResolvedValue({ data });
    const dispatch = vi.fn();

    await loadDesigns({ token: '123asdf', activeProfileId: ACTIVE_PROFILE })(
      dispatch,
      getState
    );

    expect(dispatch).toHaveBeenCalledWith(
      new StoreDesignsAction(data, ACTIVE_PROFILE)
    );
  });
});
