import { DeleteDesignAction } from '@/store/designer/actions';
import { deleteDesign } from '@/store/designer/operations';
import { deleteDesignAPI } from '@/api/designs';
import { vi, describe, it } from 'vitest';

import { ACTIVE_PROFILE, getState } from './_utils';

vi.mock('@/api/designs', () => ({
  deleteDesignAPI: vi.fn()
}));

describe('deleteDesign', () => {
  it('calls api, dispatches a DeleteDesignAction', async () => {
    deleteDesignAPI.mockResolvedValue({});
    const dispatch = vi.fn();

    await deleteDesign({
      designId: '2',
      token: '123asdf',
      activeProfileId: ACTIVE_PROFILE
    })(dispatch, getState);

    expect(deleteDesignAPI).toHaveBeenCalledWith({
      designId: '2',
      profileId: ACTIVE_PROFILE,
      token: '123asdf'
    });
    expect(dispatch).toHaveBeenCalledWith(new DeleteDesignAction('2'));
  });
});
