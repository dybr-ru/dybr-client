import { CollapseDesignerAction } from '@/store/designer/actions';

export const collapseDesigner = () => dispatch => {
  dispatch(new CollapseDesignerAction());
};
