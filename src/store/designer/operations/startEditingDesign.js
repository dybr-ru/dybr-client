import cloneDeep from 'lodash-es/cloneDeep';

import { StartEditingAction } from '@/store/designer/actions';
import { selectDesign } from '@/store/designer/selectors';

export const startEditingDesign = designID => (dispatch, getState) => {
  const design = cloneDeep(selectDesign(getState(), designID));
  dispatch(new StartEditingAction(design));
};
