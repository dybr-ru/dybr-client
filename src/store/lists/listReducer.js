import produce from 'immer';
import cloneDeep from 'lodash-es/cloneDeep';
import keyBy from 'lodash-es/keyBy';

import { defaultListsState } from '@/store/_setup/initialState';
import { LogoutAction } from '@/store/user/actions/LogoutAction';

import { ClearListsAction } from './actions';
import {
  BanProfileAction,
  HideProfileAction,
  StoreFavoritesAction,
  StoreListAction,
  StoreReadersAction,
  SubscribeToProfileAction,
  UnbanProfileAction,
  UnhideProfileAction,
  UnsubscribeFromProfileAction
} from './actions';

/*
    structure:

  {
    banned: {
      id: string,
      action: 'ban',
      kind: 'profile' | ?,
      name: string,
      scope: 'blog' | ?
      profiles: string[], // ids
    }
    hidden: {
      id: string,
      action: 'hide',
      kind: 'profile' | ?,
      name: string,
      scope: 'blog' | ?
      profiles: string[],
    },

    // these are technically subscriptions but it makes more sense to keep them here
    favorites: [],
    readers: [],

    profiles: {
      id: Profile
    }
  }

*/

function convertListPayload({ id, action, kind, name, scope, profiles = [] }) {
  const profileIDs = profiles.map(p => p.id);
  const profilesById = keyBy(profiles, 'id');

  const listData = {
    id,
    name,
    action,
    kind,
    scope,
    profiles: profileIDs
  };

  return { listData, profiles: profilesById };
}

function getListStoreName(list) {
  if (list.action === 'ban') return 'banned';
  if (list.action === 'hide') return 'hidden';
  if (list.type === 'custom') return list.name; // FUTURE FEATURE ??
}
// todo open questions:
// 1. do we really need to save all that amount of profile info???
// 2. if (!action.list) return; looks not safe
// 3. can we save only listId + profileId[] only?

export const listReducer = (state = defaultListsState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case StoreListAction.type: {
        if (!action.list) break;
        const { listData, profiles } = convertListPayload(action.list);
        const key = getListStoreName(action.list);
        // TEMP, remove when backend has fixed multiples of ban and hide lists
        if (!Object.keys(profiles).length) break;
        draft[key] = listData;
        draft.profiles = { ...draft.profiles, ...profiles };

        break;
      }
      case StoreFavoritesAction.type:
        if (!action.list) break;
        draft.favorites = action.list.map(p => p.id);
        draft.profiles = {
          ...draft.profiles,
          ...keyBy(action.list, 'id')
        };
        break;

      case StoreReadersAction.type:
        if (!action.list) break;
        draft.readers = action.list.map(p => p.id);
        draft.profiles = {
          ...draft.profiles,
          ...keyBy(action.list, 'id')
        };
        break;

      case BanProfileAction.type:
        console.log('draft.banned: ', draft.banned);
        console.log('action.profile: ', action.profile);
        if (!action.profile || !draft.banned) break; //???
        draft.banned.profiles.push(action.profile.id);
        draft.profiles[action.profile.id] = action.profile;
        break;

      case UnbanProfileAction.type:
        if (!draft.banned) break;
        draft.banned.profiles = draft.banned.profiles.filter(
          id => id !== action.profileID
        );
        break;

      case HideProfileAction.type:
        if (!action.profile || !draft.hidden) break;
        draft.hidden.profiles.push(action.profile.id);
        draft.profiles[action.profile.id] = action.profile;
        break;

      case UnhideProfileAction.type:
        if (!draft.hidden) break;
        draft.hidden.profiles = draft.hidden.profiles.filter(
          id => id !== action.profileID
        );
        break;

      case SubscribeToProfileAction.type:
        if (!action.profile) break;
        if (!draft.favorites) draft.favorites = [];
        draft.favorites.push(action.profile.id);
        draft.profiles[action.profile.id] = action.profile;
        break;

      case UnsubscribeFromProfileAction.type:
        if (!draft.favorites) break;
        draft.favorites = draft.favorites.filter(id => id !== action.profileID);
        break;
      case LogoutAction.type:
      case ClearListsAction.type:
        draft = cloneDeep(defaultListsState);
        break;
      default:
        break;
    }
  });
