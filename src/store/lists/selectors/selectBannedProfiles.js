import { createSelector } from 'reselect';
import { selectLists } from './selectLists';

export const selectBannedProfiles = createSelector(selectLists, lists =>
  (lists.banned.profiles || []).map(id => lists.profiles[id])
);
