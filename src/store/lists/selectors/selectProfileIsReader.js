import { selectReaderProfiles } from './selectReaderProfiles';
import { createSelector } from 'reselect';

export const selectProfileIsReader = createSelector(
  selectReaderProfiles,
  (state, profileId) => profileId,
  (readerProfiles, profileId) =>
    readerProfiles.findIndex(p => p.id === profileId) !== -1
);
