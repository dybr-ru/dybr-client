import { createSelector } from 'reselect';
import { selectFavoriteProfiles } from './selectFavoriteProfiles';

export const selectProfileIsFavorite = createSelector(
  selectFavoriteProfiles,
  (state, profileId) => profileId,
  (favoriteProfiles, profileId) =>
    favoriteProfiles.findIndex(p => p.id === profileId) !== -1
);
