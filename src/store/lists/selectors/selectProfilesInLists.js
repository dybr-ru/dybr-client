import { createSelector } from 'reselect';
import { selectLists } from './selectLists';

export const selectProfilesInLists = createSelector(selectLists, lists => {
  return Object.values(lists.profiles)
});
