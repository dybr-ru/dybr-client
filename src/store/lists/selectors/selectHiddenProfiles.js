import { createSelector } from 'reselect';
import { selectLists } from './selectLists';

export const selectHiddenProfiles = createSelector(selectLists, lists =>
  (lists.hidden.profiles || []).map(id => lists.profiles[id])
);
