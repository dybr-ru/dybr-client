import { createSelector } from 'reselect';
import { selectLists } from './selectLists';

export const selectBannedListId = createSelector(
  selectLists,
  lists => (lists.banned || {}).id
);
