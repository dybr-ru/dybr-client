import { selectBannedProfiles } from './selectBannedProfiles';
import { createSelector } from 'reselect';

export const selectProfileIsBanned = createSelector(
  selectBannedProfiles,
  (state, profileId) => profileId,
  (bannedProfiles, profileId) =>
    bannedProfiles.findIndex(p => p.id === profileId) !== -1
);
