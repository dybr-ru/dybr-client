import { selectHiddenProfiles } from './selectHiddenProfiles';
import { createSelector } from 'reselect';

export const selectProfileIsHidden = createSelector(
  selectHiddenProfiles,
  (state, profileId) => profileId,
  (hiddenProfiles, profileId) =>
    hiddenProfiles.findIndex(p => p.id === profileId) !== -1
);
