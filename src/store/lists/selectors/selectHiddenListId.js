import { createSelector } from 'reselect';
import { selectLists } from './selectLists';

export const selectHiddenListId = createSelector(
  selectLists,
  lists => (lists.hidden || {}).id
);
