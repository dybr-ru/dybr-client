import { selectFavoriteProfiles } from './selectFavoriteProfiles';
import { createSelector } from 'reselect';

export const selectProfileIsFavoriteBySlug = createSelector(
  selectFavoriteProfiles,
  (state, slug) => slug,
  (favoriteProfiles, slug) =>
    favoriteProfiles.findIndex(p => p.blogSlug === slug) !== -1
);
