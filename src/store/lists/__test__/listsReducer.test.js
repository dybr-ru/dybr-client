import { listReducer } from '@/store/lists/listReducer';
import {
  BanProfileAction,
  HideProfileAction,
  StoreFavoritesAction,
  StoreListAction,
  StoreReadersAction,
  SubscribeToProfileAction,
  UnbanProfileAction,
  UnhideProfileAction,
  UnsubscribeFromProfileAction
} from '../actions';
import {
  generateBanList,
  generateHiddenProfilesList,
  generateProfile,
  generateProfileList
} from './payloadMocks';
import { defaultListsState } from '@/store/_setup/initialState';
import { test, describe, expect } from 'vitest';

const EXPECTED_BANNED = {
  action: 'ban',
  id: '123',
  kind: 'profile',
  name: '',
  profiles: [0, 1],
  scope: 'blog'
};
const EXPECTED_HIDDEN = {
  action: 'hide',
  id: '456',
  kind: 'profile',
  name: '',
  profiles: [0, 1],
  scope: 'blog'
};

describe('lists reducer', () => {
  test('return given state on unknown action', () => {
    const state = { whatever: [{ listID: 1, data: 'some data' }] };
    const newState = listReducer(state, {
      type: 'some_action',
      payload: { some: 'data' }
    });
    expect(newState).toEqual(state);
  });

  test('StoreListAction adds sorted ban list payload', () => {
    const banned = generateBanList(2);

    const newState = listReducer(defaultListsState, new StoreListAction(banned));
    const expectedState = {
      ...defaultListsState,
      banned: EXPECTED_BANNED,
      profiles: {
        0: banned.profiles[0],
        1: banned.profiles[1]
      }
    };
    expect(newState).toEqual(expectedState);
  });

  test('StoreListAction adds sorted hidden list payload', () => {
    const hidden = generateHiddenProfilesList(2);

    const newState = listReducer(defaultListsState, new StoreListAction(hidden));
    const expectedState = {
      ...defaultListsState,
      hidden: EXPECTED_HIDDEN,
      profiles: {
        0: hidden.profiles[0],
        1: hidden.profiles[1]
      }
    };
    expect(newState).toEqual(expectedState);
  });

  /** this is incorrect, there should not be two lists for one type, backend should fix it */
  test('StoreListAction combines data together on subsequent updates', () => {
    const banned = generateBanList(2);
    const hidden = generateHiddenProfilesList(2);

    const firstState = listReducer(defaultListsState, new StoreListAction(banned));
    const secondState = listReducer(firstState, new StoreListAction(hidden));

    const expectedState = {
      ...defaultListsState,
      banned: EXPECTED_BANNED,
      hidden: EXPECTED_HIDDEN,
      profiles: {
        0: hidden.profiles[0],
        1: hidden.profiles[1]
      }
    };
    expect(secondState).toEqual(expectedState);
  });

  test('BanProfileAction adds the profile to the banned list', () => {
    let profile = generateProfile();
    let banned = generateBanList(2);

    const firstState = listReducer(defaultListsState, new StoreListAction(banned));
    const secondState = listReducer(firstState, new BanProfileAction(profile));

    const expectedState = {
      ...defaultListsState,
      banned: { ...EXPECTED_BANNED, profiles: [0, 1, profile.id] },
      profiles: {
        0: banned.profiles[0],
        1: banned.profiles[1],
        [profile.id]: profile
      }
    };
    expect(secondState).toEqual(expectedState);
  });

  test('UnbanProfileAction removes the profile from the banned list but not profile list', () => {
  // because we might need that cache for other lists

    let banned = generateBanList(2);

    const firstState = listReducer(defaultListsState, new StoreListAction(banned));
    const secondState = listReducer(
      firstState,
      new UnbanProfileAction(banned.profiles[0])
    );

    expect(secondState).toEqual({
      ...defaultListsState,
      banned: { ...EXPECTED_BANNED, profiles: [1] },
      profiles: {
        0: banned.profiles[0],
        1: banned.profiles[1]
      }
    });
  });

  test('HideProfileAction adds the profile to the hidden list', () => {
    let profile = generateProfile();
    let hidden = generateHiddenProfilesList(2);

    const firstState = listReducer(defaultListsState, new StoreListAction(hidden));
    const secondState = listReducer(firstState, new HideProfileAction(profile));

    expect(secondState).toEqual({
      ...defaultListsState,
      hidden: { ...EXPECTED_HIDDEN, profiles: [0, 1, profile.id] },
      profiles: {
        0: hidden.profiles[0],
        1: hidden.profiles[1],
        [profile.id]: profile
      }
    });
  });

  test('UnhideProfileAction removes the profile from the hidden list but not profile list', () => {
  // because we might need that cache for other lists

    let hidden = generateHiddenProfilesList(2);

    const firstState = listReducer(defaultListsState, new StoreListAction(hidden));
    const secondState = listReducer(
      firstState,
      new UnhideProfileAction(hidden.profiles[0])
    );

    expect(secondState).toEqual({
      ...defaultListsState,
      hidden: { ...EXPECTED_HIDDEN, profiles: [1] },
      profiles: {
        0: hidden.profiles[0],
        1: hidden.profiles[1]
      }
    });
  });

  test('StoreFavoritesAction adds sorted payload to the favorites list', () => {
    let list = generateProfileList(2);

    const newState = listReducer(
      { profiles: { aaa: 'aaa' } },
      new StoreFavoritesAction(list)
    );
    expect(newState).toEqual({
      favorites: list.map(p => p.id),
      profiles: {
        aaa: 'aaa',
        0: list[0],
        1: list[1]
      }
    });
  });

  test('StoreReadersAction adds sorted payload to the readers list', () => {
    let list = generateProfileList(2);

    const newState = listReducer(
      { profiles: { aaa: 'aaa' } },
      new StoreReadersAction(list)
    );
    expect(newState).toEqual({
      readers: list.map(p => p.id),
      profiles: {
        aaa: 'aaa',
        0: list[0],
        1: list[1]
      }
    });
  });

  test('SubscribeToProfileAction action adds the profile to the favorites list', () => {
    let profile = generateProfile();
    let list = generateProfileList(2);

    const firstState = listReducer(defaultListsState, new StoreFavoritesAction(list));
    const secondState = listReducer(
      firstState,
      new SubscribeToProfileAction(profile)
    );

    expect(secondState).toEqual({
      ...defaultListsState,
      favorites: [0, 1, profile.id],
      profiles: {
        0: list[0],
        1: list[1],
        [profile.id]: profile
      }
    });
  });

  test('UnsubscribeFromProfileAction action removes the profile from the favorites list', () => {
    let list = generateProfileList(2);

    const firstState = listReducer(defaultListsState, new StoreFavoritesAction(list));
    const secondState = listReducer(
      firstState,
      new UnsubscribeFromProfileAction(list[0])
    );

    expect(secondState).toEqual({
      ...defaultListsState,
      favorites: [1],
      profiles: {
        0: list[0],
        1: list[1]
      }
    });
  });
})
