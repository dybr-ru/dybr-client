import {
  banProfile,
  hideProfile,
  loadCustomLists,
  loadFavorites,
  loadLists,
  loadReaders,
  subscribeToProfile,
  unbanProfile,
  unhideProfile,
  unsubscribeFromProfile
} from '../operations';
import {
  BanProfileAction,
  HideProfileAction,
  StoreFavoritesAction,
  StoreListAction,
  StoreReadersAction,
  SubscribeToProfileAction,
  UnbanProfileAction,
  UnhideProfileAction,
  UnsubscribeFromProfileAction
} from '../actions';

import {
  banProfileApi,
  getListsApi,
  hideProfileFromFeedApi,
  removeProfileFromListApi
} from '@/api/lists';

import {
  addToFavoritesApi,
  getFavoritesApi,
  getReadersApi,
  removeFromFavoritesApi
} from '@/api/favorites';
import { getState } from '@/store/lists/__test__/_utils';

jest.mock('api/lists', () => ({
  getListsApi: jest.fn(),
  banProfileApi: jest.fn(),
  hideProfileFromFeedApi: jest.fn(),
  removeProfileFromListApi: jest.fn()
}));

jest.mock('api/favorites', () => ({
  getFavoritesApi: jest.fn(),
  getReadersApi: jest.fn(),
  addToFavoritesApi: jest.fn(),
  removeFromFavoritesApi: jest.fn()
}));

describe('load custom lists', () => {
  it('if data is returned, dispatches a StoreListAction for each list', async () => {
    const data = [{ id: '1' }, { id: '2' }];
    getListsApi.mockResolvedValue({ data });

    const dispatch = jest.fn();
    await loadCustomLists({ profileId: '1', token: '123asdf' })(
      dispatch,
      getState
    );

    expect(dispatch).toHaveBeenCalledTimes(2);
    expect(dispatch).toHaveBeenCalledWith(new StoreListAction(data[0]));
    expect(dispatch).toHaveBeenCalledWith(new StoreListAction(data[1]));
  });
});

describe('load favorites', () => {
  it('if data is returned, dispatches a StoreFavoritesAction', async () => {
    const data = [{ id: '1' }, { id: '2' }];
    getFavoritesApi.mockResolvedValue({ data });

    const dispatch = jest.fn();
    await loadFavorites({ profileId: 1, token: '123asdf' })(dispatch, getState);

    expect(dispatch).toHaveBeenCalledWith(new StoreFavoritesAction(data));
  });
});

describe('load readers', () => {
  it('if data is returned, dispatches a StoreReadersAction', async () => {
    const data = [{ id: '1' }, { id: '2' }];
    getReadersApi.mockResolvedValue({ data });

    const dispatch = jest.fn();
    await loadReaders({ profileId: '1', token: '123asd' })(dispatch, getState);

    expect(dispatch).toHaveBeenCalledWith(new StoreReadersAction(data));
  });
});

describe('load all lists', () => {
  it('dispatches a StoreListAction for each list, StoreFavoritesAction, and StoreReadersAction', async () => {
    const data = [{ id: '1' }, { id: '2' }];
    getListsApi.mockResolvedValue({ data });
    getFavoritesApi.mockResolvedValue({ data });
    getReadersApi.mockResolvedValue({ data });

    const dispatch = jest.fn();
    await loadLists({ profileId: '1', token: '123asdf' })(dispatch, getState);

    expect(dispatch).toHaveBeenCalledTimes(4);
    expect(dispatch).toHaveBeenCalledWith(new StoreListAction(data[0]));
    expect(dispatch).toHaveBeenCalledWith(new StoreListAction(data[1]));
    expect(dispatch).toHaveBeenCalledWith(new StoreFavoritesAction(data));
    expect(dispatch).toHaveBeenCalledWith(new StoreReadersAction(data));
  });
});

describe('subscribe to profile', () => {
  it('if data is returned, dispatches a SubscribeToProfileAction', async () => {
    const profile = { id: '1' };
    addToFavoritesApi.mockResolvedValue({});

    const dispatch = jest.fn();
    await subscribeToProfile({
      profile,
      token: '123asdf',
      activeProfileId: '1'
    })(dispatch, getState);

    expect(dispatch).toHaveBeenCalledWith(
      new SubscribeToProfileAction(profile)
    );
  });
});

describe('unsubscribe from profile', () => {
  it('if data is returned, dispatches an UnsubscribeFromProfileAction', async () => {
    const profile = { id: '1' };
    removeFromFavoritesApi.mockResolvedValue({});

    const dispatch = jest.fn();

    await unsubscribeFromProfile({
      profile,
      token: '123asdf',
      activeProfileId: '1'
    })(dispatch, getState);

    expect(dispatch).toHaveBeenCalledWith(
      new UnsubscribeFromProfileAction(profile)
    );
  });
});

describe('hide profile', () => {
  it('if data is returned and the list exists, dispatches a HideProfileAction', async () => {
    const profile = { id: '1' };
    hideProfileFromFeedApi.mockResolvedValue({});

    const dispatch = jest.fn();
    await hideProfile({ profile, token: '123aasdf' })(dispatch, () => ({
      ...getState(),
      lists: { hidden: { id: '1' } }
    }));

    expect(dispatch).toHaveBeenCalledWith(new HideProfileAction(profile));
  });

  it('if the list does not exist, dispatches a StoreListAction', async () => {
    const profile = { id: '1' };
    hideProfileFromFeedApi.mockResolvedValue({ data: { id: '2' } });

    const dispatch = jest.fn();
    await hideProfile({ profile, token: '1234asdf' })(dispatch, () => ({
      ...getState(),
      lists: { hidden: {} }
    }));

    expect(dispatch).toHaveBeenCalledWith(new StoreListAction({ id: '2' }));
  });
});

describe('unhide profile', () => {
  it('if data is returned, dispatches an UnhideProfileAction', async () => {
    const profile = { id: '1' };
    removeProfileFromListApi.mockResolvedValue({});

    const dispatch = jest.fn();
    await unhideProfile({ profile, token: '123asdf' })(dispatch, () => ({
      ...getState(),
      lists: {
        hidden: {
          id: '1'
        }
      }
    }));

    expect(dispatch).toHaveBeenCalledWith(new UnhideProfileAction(profile));
  });
});

describe('ban profile', () => {
  it('if data is returned and the list exists, dispatches a BanProfileAction', async () => {
    const profile = { id: '1' };
    banProfileApi.mockResolvedValue({});

    const dispatch = jest.fn();
    await banProfile({ profile, token: '123asdf' })(dispatch, () => ({
      ...getState(),
      lists: { banned: { id: '1' } }
    }));

    expect(dispatch).toHaveBeenCalledWith(new BanProfileAction(profile));
  });

  it('if the list does not exist, dispatches a StoreListAction', async () => {
    const profile = { id: '1' };
    banProfileApi.mockResolvedValue({ data: { id: '2' } });

    const dispatch = jest.fn();
    await banProfile({ profile, token: '123asdf' })(dispatch, () => ({
      ...getState(),
      lists: { banned: {} }
    }));

    expect(dispatch).toHaveBeenCalledWith(new StoreListAction({ id: '2' }));
  });
});

describe('unban profile', () => {
  it('if data is returned, dispatches an UnbanProfileAction', async () => {
    const profile = { id: '1' };
    removeProfileFromListApi.mockResolvedValue({});

    const dispatch = jest.fn();
    await unbanProfile({ profile, token: '123asdf' })(dispatch, () => ({
      ...getState(),
      lists: {
        banned: {
          id: '1'
        }
      }
    }));
    expect(dispatch).toHaveBeenCalledWith(new UnbanProfileAction(profile));
  });
});
