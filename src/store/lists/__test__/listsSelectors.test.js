import { generateProfileList } from './payloadMocks';
import { selectBannedProfiles } from '../selectors/selectBannedProfiles';
import { selectHiddenProfiles } from '../selectors/selectHiddenProfiles';
import { selectReaderProfiles } from '../selectors/selectReaderProfiles';
import { selectFavoriteProfiles } from '../selectors/selectFavoriteProfiles';

let state;

describe('lists selectors', () => {
  beforeEach(() => {
    state = {
      lists: {
        banned: { profiles: [0, 1] },
        hidden: { profiles: [1, 2] },
        favorites: [3, 4],
        readers: [0, 3],
        profiles: generateProfileList(5)
      }
    };
  });

  test('selectBannedProfiles selector returns profiles', () => {
    const selected = selectBannedProfiles(state);
    expect(selected).toEqual([
      state.lists.profiles[0],
      state.lists.profiles[1]
    ]);
  });

  test('selectHiddenProfiles selector returns profiles', () => {
    const selected = selectHiddenProfiles(state);
    expect(selected).toEqual([
      state.lists.profiles[1],
      state.lists.profiles[2]
    ]);
  });

  test('selectFavoritesProfiles selector returns profiles with isReader and isFavorite markers', () => {
    const selected = selectFavoriteProfiles(state);
    expect(selected).toEqual([
      { ...state.lists.profiles[3], isReader: true, isFavorite: true },
      { ...state.lists.profiles[4], isReader: false, isFavorite: true }
    ]);
  });

  test('selectReadersProfiles selector returns profiles with isReader and isFavorite markers', () => {
    const selected = selectReaderProfiles(state);
    expect(selected).toEqual([
      { ...state.lists.profiles[0], isReader: true, isFavorite: false },
      { ...state.lists.profiles[3], isReader: true, isFavorite: true }
    ]);
  });
});
