
import { initialState } from '@/store/_setup/initialState';

export const ACTIVE_PROFILE = '1';
export const getState = () => ({
  ...initialState,
  user: { activeProfile: ACTIVE_PROFILE, token: '123' }
});
