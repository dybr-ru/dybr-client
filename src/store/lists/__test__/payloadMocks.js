import { faker } from "@faker-js/faker";


export function generateProfile(id = faker.number.int({min: 100000, max: 999999)) {
  return {
    type: 'profiles',
    id,
    blogSlug: faker.internet.userName(),
    blogTitle: faker.lorem.words(),
    createdAt: '2018-04-29T20:09:34Z',
    nickname: faker.internet.userName(),
    description: faker.lorem.words(),
    settings: {
      permissions: {},
      avatar: faker.internet.url(),
      subtext: faker.lorem.words(),
      notifications: {},
      currentDesign: faker.number.int(10),
    },
    updatedAt: '2019-02-17T22:23:04Z',
    meta: { entries: { public: faker.number.int(10) } }
  };
}

export function generateListsWithProfiles(banned, hidden) {}

export function generateBanList(n) {
  const profiles = [];
  for (let i = 0; i < n; i++) profiles.push(generateProfile(i));
  return {
    type: 'profile-list',
    id: '123',
    action: 'ban',
    kind: 'profile',
    scope: 'blog',
    name: '',
    profiles
  };
}

export function generateHiddenProfilesList(n) {
  const profiles = [];
  for (let i = 0; i < n; i++) profiles.push(generateProfile(i));
  return {
    type: 'profile-list',
    id: '456',
    action: 'hide',
    kind: 'profile',
    scope: 'blog',
    name: '',
    profiles
  };
}
export function generateProfileList(n) {
  let profiles = [];
  for (let i = 0; i < n; i++) {
    profiles.push(generateProfile(i));
  }
  return profiles;
}
