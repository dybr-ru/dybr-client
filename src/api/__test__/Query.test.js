import { query } from '../Query';
import axiosMock from 'axios';
import { API_URL } from '@/configs/dybr';
import { vi, test, describe } from 'vitest';

vi.mock('axios');

beforeEach(() => {
  vi.resetModules();
  vi.clearAllMocks();
});

describe('Query Builder', () => {
  it('Creates POST request', async () => {
    const response = { meta: {}, data: '{"data": []}' };
    const expected = {
      meta: {},
      dataOriginal: '{"data": []}',
      data: [],
      status: 200
    };
    axiosMock.mockImplementation(() =>
      Promise.resolve({ ...response, status: 200 })
    );

    const res = await query({ method: 'POST', resource: 'entries', id: 1 });
    expect(axiosMock).toHaveBeenCalledTimes(1);

    expect(res).toEqual(expected);
  });

  it('Serializes data correctly', async () => {
    const response = { meta: {}, data: '{"data": []}' };
    const expected = {
      meta: {},
      data: [],
      dataOriginal: '{"data": []}',
      status: 200
    };
    axiosMock.mockImplementation(() =>
      Promise.resolve({ ...response, status: 200 })
    );

    const res = await query({
      method: 'POST',
      resource: 'entries',
      queryParams: {
        camelCase: 'AA',
        profile: { id: 1, type: 'profiles' },
        relationshipNames: ['profile']
      },
      id: 1
    });
    expect(axiosMock).toHaveBeenCalledTimes(1);
    // expect(axiosMock).toHaveBeenCalledWith({
    //   data: `{"data":{"type":"entries","attributes":{"camel-case":"AA"},"relationships":{"profile":{"data":{"id":1,"type":"profiles"}}}},"included":[{"id":1,"type":"profiles","attributes":{}}]}`,
    //   method: 'POST',
    //   url: API_URL + '/entries/1?nonce=TIMESTAMP' // todo - somehow ignore timestamp
    // });
    expect(res).toEqual(expected);
  });

  it('Deserializes data correctly', async () => {
    const response = {
      meta: {},
      data:
        '{"data":[{"type":"entries","id":"13634","attributes":{"content":"test","created-at":"2019-02-03T10:34:01Z","published-at":"2019-02-03T10:34:01Z","state":"published","title":"test","updated-at":"2019-02-03T10:34:01Z"},"links":{"self":"//dybr.ru/v2//entries/13634"},"meta":{"commenters":0,"comments":0,"privacy":"registered"}}]}'
    };
    const expected = {
      meta: {},
      dataOriginal:
        '{"data":[{"type":"entries","id":"13634","attributes":{"content":"test","created-at":"2019-02-03T10:34:01Z","published-at":"2019-02-03T10:34:01Z","state":"published","title":"test","updated-at":"2019-02-03T10:34:01Z"},"links":{"self":"//dybr.ru/v2//entries/13634"},"meta":{"commenters":0,"comments":0,"privacy":"registered"}}]}',
      data: [
        {
          id: '13634',
          content: 'test',
          createdAt: '2019-02-03T10:34:01Z',
          publishedAt: '2019-02-03T10:34:01Z',
          state: 'published',
          title: 'test',
          updatedAt: '2019-02-03T10:34:01Z',
          links: {
            self: '//dybr.ru/v2//entries/13634'
          },
          meta: {
            commenters: 0,
            comments: 0,
            privacy: 'registered'
          },
          type: 'entries'
        }
      ],
      status: 200
    };
    axiosMock.mockImplementation(() =>
      Promise.resolve({ ...response, status: 200 })
    );

    const res = await query({ method: 'POST', resource: 'entries', id: 1 });

    expect(res).toEqual(expected);
  });

  it('Creates GET request', async () => {
    const response = { data: '{"data": []}' };
    const expected = {
      meta: {},
      data: [],
      dataOriginal: '{"data": []}',
      status: 200
    };
    axiosMock.get.mockImplementation(() =>
      Promise.resolve({ ...response, status: 200 })
    );

    const res = await query({ method: 'GET', resource: 'entries', id: 1 });
    expect(axiosMock.get).toHaveBeenCalledTimes(1);
    expect(axiosMock.get).toHaveBeenCalledWith(API_URL + '/entries/1', {});
    expect(res).toEqual(expected);
  });

  it('Adds parameters to GET request', async () => {
    const response = { data: '{"data": []}' };
    const expected = {
      meta: {},
      data: [],
      dataOriginal: '{"data": []}',
      status: 200
    };
    axiosMock.get.mockImplementation(() =>
      Promise.resolve({ ...response, status: 200 })
    );

    const res = await query({
      method: 'GET',
      resource: 'entries',
      queryParams: { param: 123, other: 'qqq' },
      id: 1
    });
    expect(axiosMock.get).toHaveBeenCalledTimes(1);
    expect(axiosMock.get).toHaveBeenCalledWith(
      API_URL + '/entries/1?other=qqq&param=123',
      {}
    );
    expect(res).toEqual(expected);
  });
});
