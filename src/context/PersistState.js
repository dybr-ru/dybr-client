export const loadContext = node => {
  try {
    const serialized = localStorage.getItem(node);
    if (serialized === null) {
      return undefined;
    }
    return JSON.parse(serialized);
  } catch (err) {
    console.log('loadContext', err);

    localStorage.clear();
    return undefined;
  }
};
export const saveContext = (state, node) => {
  try {
    const serialized = JSON.stringify(state);
    if (serialized) localStorage.setItem(node, serialized);
  } catch (err) {
    console.log('saveContext', err);
  }
};
