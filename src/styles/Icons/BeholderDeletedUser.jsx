import React from 'react';

const Icon = ({ className }) => (
  <svg
    viewBox="0 0 21 21"
    className={`icon icon-beholder-deleted-user ${className}`}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path className="square main-outline" d="M 0,0 H 21 V 21 H 0 Z" />
    <path
      className="cross main-outline"
      d="M 15.096194,4.4895924 10.5,9.0857864 5.9038059,4.4895924 4.4895924,5.9038059 9.0857864,10.5 4.4895924,15.096194 5.9038059,16.510408 10.5,11.914214 15.096194,16.510408 16.510408,15.096194 11.914214,10.5 16.510408,5.9038059 Z"
    />
  </svg>
);

export default Icon;
