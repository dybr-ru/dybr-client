import React from 'react';

const Icon = ({ className }) => (
  <svg
    viewBox="0 0 898.50092 931.72034"
    className={`icon icon-back-to-top ${className}`}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      className="back-to-top-arrow-inside filling-area"
      d="m 449.28995,191.93051 312.54679,404.5148 H 599.53857 v 273.6678 H 298.96562 V 596.44531 H 136.66921 Z"
    />
    <path
      className="back-to-top-arrow filling-outline"
      d="M 449.21715,84.759115 0.01482761,666.17445 H 231.75506 v 265.54589 h 434.99413 v -35.8354 -229.71049 h 231.74024 z m 0.0717,117.234945 303.27102,392.5096 H 595.07839 V 860.04954 H 303.42585 V 594.50366 H 145.94608 Z"
    />
    <path
      className="back-to-top-topline main-outline"
      d="M 0,0 H 898.50093 V 71.670795 H 0 Z"
    />
  </svg>
);

export default Icon;
