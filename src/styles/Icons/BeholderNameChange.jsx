import React from 'react';

const Icon = ({ className }) => (
  <svg
    viewBox="0 0 21 24"
    className={`icon icon-beholder-name-change ${className}`}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      className="squares main-outline"
      d="m 13,0 h 8 V 3 H 13 Z M 4,21 h 13 v 3 H 4 Z M 0,0 H 3 V 3 H 0 Z M 5,0 H 8 V 3 H 5 Z m 5,0 h 3 v 3 h -3 z"
    />
    <path
      className="arrow main-outline"
      d="m 10.603991,19.23247 7.15039,-8.953536 H 14.065547 V 6.1896461 H 7.1413218 V 6.7414947 10.278934 H 3.4524869 Z m -0.001,-2.008755 -4.4688241,-5.783793 H 8.454624 V 7.5269955 h 4.297621 v 3.9129265 h 2.320529 z"
    />
  </svg>
);

export default Icon;
