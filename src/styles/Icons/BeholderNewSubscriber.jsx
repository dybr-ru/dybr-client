import React from 'react';

const Icon = ({ className }) => (
  <svg
    viewBox="0 0 21 21"
    className={`icon icon-beholder-new-subscriber ${className}`}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path className="square main-outline" d="M 0,0 H 21 V 21 H 0 Z" />
    <path
      className="plus main-outline"
      d="M 9.5 3 L 9.5 9.5 L 3 9.5 L 3 11.5 L 9.5 11.5 L 9.5 18 L 11.5 18 L 11.5 11.5 L 18 11.5 L 18 9.5 L 11.5 9.5 L 11.5 3 L 9.5 3 z"
    />
  </svg>
);

export default Icon;
