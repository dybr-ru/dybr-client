import React from 'react';

export const ComplainIcon = ({ className }) => (
  <svg
    viewBox="0 0 3.1029781 11.442233"
    className={`icon icon-complain ${className}`}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      className="exclamation-point main-outline"
      d="m 0.048484,0 h 3.00601 L 2.553493,7.3857347 H 0.549486 Z m 1.503005,11.442233 q -0.662615,0 -1.115133,-0.420195 Q 0,10.585681 0,9.9715497 q 0,-0.614131 0.436356,-1.018165 0.436357,-0.420195 1.115133,-0.420195 0.678777,0 1.115133,0.420195 0.436356,0.404034 0.436356,1.018165 0,0.6141313 -0.452517,1.0504883 -0.436357,0.420195 -1.098972,0.420195 z"
    />
  </svg>
);
