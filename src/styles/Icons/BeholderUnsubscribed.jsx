import React from 'react';

const Icon = ({ className }) => (
  <svg
    viewBox="0 0 21 21"
    className={`icon icon-beholder-unsubscribed ${className}`}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path className="square main-outline" d="M 0,0 H 21 V 21 H 0 Z" />
    <path className="minus main-outline" d="m 18,9.5 v 2 H 3 v -2 z" />
  </svg>
);

export default Icon;
