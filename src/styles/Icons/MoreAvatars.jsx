import React from 'react';

const Icon = ({ className }) => (
  <svg
    viewBox="0 0 18 30"
    className={`icon icon-more-avatars ${className}`}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      className="rectangle main-outline"
      d="m 5,0 h 8 c 2.77,0 5,2.23 5,5 v 20 c 0,2.77 -2.23,5 -5,5 H 5 C 2.23,30 0,27.77 0,25 V 5 C 0,2.23 2.23,0 5,0 Z"
    />
    <path
      className="elipses main-outline"
      d="M 9 5 A 2 2 0 0 0 7 7 A 2 2 0 0 0 9 9 A 2 2 0 0 0 11 7 A 2 2 0 0 0 9 5 z M 9 13 A 2 2 0 0 0 7 15 A 2 2 0 0 0 9 17 A 2 2 0 0 0 11 15 A 2 2 0 0 0 9 13 z M 9 21 A 2 2 0 0 0 7 23 A 2 2 0 0 0 9 25 A 2 2 0 0 0 11 23 A 2 2 0 0 0 9 21 z"
    />
  </svg>
);

export default Icon;
