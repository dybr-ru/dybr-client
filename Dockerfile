# stage 1: builder image
FROM node:12 as builder

# Copy lint rules/dependencies/package info
COPY *.json ./
COPY .env.local ./

# copy source from project dir
COPY ./config/ ./config/
COPY ./public/ ./public/
COPY ./src/ ./src/

RUN npm install
RUN npm run localhost:build

# stage 2: actual image
FROM nginx:latest

# labels
LABEL maintainer="Oleg `Kanedias` Chernovskiy"
LABEL role="frontend"

COPY --from=builder /build /usr/share/nginx/html

